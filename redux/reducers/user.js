
let defaultState = {
    branch: { sportsClubName: "", branchName: "" },
    member: undefined
}

const app = (state = defaultState, action) => {
    switch (action.type) {
        case 'BRANCH_INIT':
            return {
                ...state,
                branch: action.payload
            }
        case 'MEMBER_INIT':
            return {
                ...state,
                member: action.payload
            }
        default:
            return state;
    }
}

export default app;
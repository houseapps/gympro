
let defaultState = {
    isLoading: false,
    modal: {
        isShow: false,
        header: "",
        message: ""
    }
}

const app = (state = defaultState, action) => {
    switch (action.type) {
        case 'LOADING':
            return {
                ...state,
                isLoading: true,
                modal: {
                    isShow: false,
                    header: "",
                    message: ""
                }
            }
        case 'LOADED':
            return {
                ...state,
                isLoading: false,
                modal: {
                    isShow: false,
                    header: "",
                    message: ""
                }
            }
        case 'ALERT':
            return {
                ...state,
                modal: {
                    isShow: true,
                    header: action.payload.header,
                    message: action.payload.message
                }
            }
        default:
            return state;
    }
}

export default app;
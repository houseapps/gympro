
export default headerSelector = function (state) {
    return {
        title: state.user.branch.sportsClubName + " " + state.user.branch.branchName,
        iconUrl: state.user.branch.logoPath
    };
}
import store from '../store';
import { AsyncStorage } from 'react-native';
import WebClient, { setAccessToken, setBranchId } from '../../helper/webclient';

export function isLoading() {
    store.dispatch({
        type: "LOADING",
        payload: {}
    });
}

export function isLoaded() {
    store.dispatch({
        type: "LOADED",
        payload: {}
    });
}

export function alert(header, message) {
    store.dispatch({
        type: "ALERT",
        payload: { header, message }
    });
}

export function login(data, activationCb) {
    let client = new WebClient({
        "baseURL": "https://api.fitnessonline.net",
        "crossDomain": true,
        "headers": {
            "content-type": "application/json"
        }
    });
    client.post("v1/Mobile/Login", data, true, false)
        .then(response => {
            if (response.data.errorCode != 300 && response.data.errorCode != 100 ) {
                setAccessToken(response.data.data.access_token);
                AsyncStorage.setItem("login_status", JSON.stringify(true));
                let client = new WebClient();
                client.get("Mobile/MemberBranch").then(response => {
                    store.dispatch({
                        type: "BRANCH_INIT",
                        payload: response.data.data
                    });

                    setBranchId(response.data.data.branchID);
                    client.get(`Mobile/MemberInfo`).then(response => {
                        store.dispatch({
                            type: "MEMBER_INIT",
                            payload: response.data.data
                        });
                    });
                });
            }
            else {
                activationCb(response.data);
            }
            
        });
}
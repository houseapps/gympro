import React, { Component } from "react";
import { AppRegistry, View, Alert, KeyboardAvoidingView, StatusBar, Platform, ActivityIndicator, AsyncStorage, Text } from 'react-native';
import { Provider, connect } from 'react-redux'
import { Spinner } from 'native-base';
import { DrawerNavigator } from "react-navigation";
import OneSignal from 'react-native-onesignal';
// import I18n, { getLanguages } from 'react-native-i18n';

import store from './redux/store';
import App from './views/App';


import Auth from './views/auth';
import * as AppActions from './redux/actions/app';

console.disableYellowBox = true;

let AppWithConnect = connect()(App);
let AuthWithConnect = connect()(Auth);

let my = this;
export default class AppContainer extends Component {   
    constructor(props) {
        super(props);
        my = this;
        this.state = { ...store.getState().app, ...store.getState().user };
        this._storeChanged = this._storeChanged.bind(this);
        store.subscribe(this._storeChanged);
    }

    _storeChanged() {
        this.setState({ ...store.getState().app, ...store.getState().user });
    }   

    componentWillMount() {
        /*
        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('opened', this.onOpened);
        OneSignal.addEventListener('registered', this.onRegistered);
        OneSignal.addEventListener('ids', this.onIds);
        */
    }

    /*
    componentWillUnmount() {
        OneSignal.removeEventListener('received', this.onReceived);
        OneSignal.removeEventListener('opened', this.onOpened);
        OneSignal.removeEventListener('registered', this.onRegistered);
        OneSignal.removeEventListener('ids', this.onIds);
    }

    onReceived(notification) {
        console.log("Notification received: ", notification);
    }

    onOpened(openResult) {
      console.log('Message: ', openResult.notification.payload.body);
      console.log('Data: ', openResult.notification.payload.additionalData);
      console.log('isActive: ', openResult.notification.isAppInFocus);
      console.log('openResult: ', openResult);
    }

    onRegistered(notifData) {
        console.log("Device had been registered for push notifications!", notifData);
    }

    onIds(device) {
        console.log('Device info: ', device);
    }    
    */

    render() {
        return (
            <Provider store={store}>
                <View style={{ width: "100%", height: "100%" }}>
                    {
                        this.state.member != undefined ?
                            <KeyboardAvoidingView
                                pointerEvents={this.state.isLoading ? "none" : "auto"}
                                keyboardVerticalOffset={Platform.OS == "ios" ? 0 : -500}
                                behavior="padding"
                                style={{ width: "100%", height: "100%" }}>
                                <AppWithConnect />
                            </KeyboardAvoidingView> :
                            <AuthWithConnect
                                pointerEvents={this.state.isLoading ? "none" : "auto"} />
                    }
                    {
                        this.state.modal.isShow ?
                            Alert.alert(
                                this.state.modal.header,
                                this.state.modal.message,
                                [
                                    { text: 'Tamam' },
                                ],
                                { cancelable: false }
                            ) :
                            null
                    }
                    {
                        this.state.isLoading ?
                            <View style={{ width: "100%", height: "100%", position: 'absolute', left: 0, top: 0, opacity: 0.5, backgroundColor: 'gray' }} />
                            :
                            null
                    }
                    {
                        this.state.isLoading ?
                            <Spinner style={{ position: "absolute", top: "45%", left: "45%" }} color='black' />
                            :
                            null
                    }
                </View>
            </Provider>
        );
    }
}

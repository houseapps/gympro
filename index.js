import React, { Component } from "react";
import { AppRegistry, View, Text, Image } from 'react-native';
import { Provider, connect } from 'react-redux'
import { Spinner } from 'native-base';

import AppIndex from './AppIndex';


console.disableYellowBox = true;

class AppContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {showSplash: true}
    }

    render() {
        return (
          <AppIndex />
        );
    }
}

AppRegistry.registerComponent('gympro', () => AppContainer);

import React, { Component } from 'react';
import { StyleSheet, View, Platform } from 'react-native';
import { Text, Picker, Item, Icon } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Divider } from 'react-native-elements';

import WebClient from '../../../helper/webclient';

export default class Olculerim extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: "heightCM",
            ilkOlcum: { measurementDate: "", heightCM: "" },
            oncekiOlcum: { measurementDate: "", heightCM: "" },
            sonOlcum: { measurementDate: "", heightCM: "" }
        }

        this.webClient = new WebClient();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isLoaded) {
            this.webClient.get(`Mobile/Measurements/Comparison/1`)
                .then(response => {
                    this.setState({ ilkOlcum: response.data.data[0] })
                    this.webClient.get(`Mobile/Measurements/Comparison/2`)
                        .then(response => {
                            this.setState({ oncekiOlcum: response.data.data[0] })
                            this.webClient.get(`Mobile/Measurements/Comparison/3`)
                                .then(response => {
                                    this.setState({ sonOlcum: response.data.data[0] })
                                });
                        });
                });
        }
    }

    render() {
        return (
            <View>
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                    <Text style={{ marginLeft: 10, fontSize: 18, fontWeight: "400", color: "rgb(65,159,221)" }}>Ölçümlerim</Text>
                    <View style={{
                        flexDirection: "row", alignItems: "center",
                        backgroundColor: "rgb(65,159,221)", borderRadius: 5,
                        marginTop: 10, marginBottom: 10, marginRight: 10, height: 40
                    }}>
                        <Picker info style={{ width: (Platform.OS === 'ios') ? undefined : 150 }}
                            textStyle={{ color: 'white' }}
                            iosHeader="Seçim yapınız"
                            headerBackButtonText="Geri"
                            mode="dropdown"
                            selectedValue={this.state.selectedValue}
                            onValueChange={(value) => { this.setState({ selectedValue: value }) }}>
                            <Item label="Boy(cm)" value="heightCM" />
                            <Item label="Ağırlık(kg)" value="weightKG" />
                            <Item label="Beden Kitle Endeksi" value="bmi" />
                            <Item label="Yağ Oranı(%)" value="fatPercent" />
                            <Item label="Yağ(kg)" value="fatKG" />
                            <Item label="Sıvı Oranı(%)" value="tbwPercent" />
                            <Item label="Sıvı(kg)" value="tbwKG" />
                            <Item label="Kas(kg)" value="muscleKG" />
                        </Picker>
                        {
                            Platform.OS == "ios" ?
                                <Icon style={{ marginLeft: -10, marginRight: 10, fontSize: 14, color: "white" }} name="ios-arrow-down" />
                                : null
                        }
                    </View>
                </View>
                <Grid>
                    <Row style={{ margin: 10 }}>
                        <Col style={{ flex: 2 }}>
                            <Text note style={{ fontWeight: "bold" }}>İlk Ölçüm</Text>
                        </Col>
                        <Col style={{ flex: 1 }}>
                            <Text note style={{ fontWeight: "bold" }}>{Number.parseFloat(this.state.ilkOlcum[this.state.selectedValue]).toFixed(2)}</Text>
                        </Col>
                        <Col style={{ flex: 2 }}>
                            <Text note style={{ fontWeight: "bold" }}>{this.state.ilkOlcum.measurementDateFormatted}</Text>
                        </Col>
                    </Row>
                    <Divider style={{ marginTop: 5, marginBottom: 5, backgroundColor: 'lightgray', height: 1, width: "100%" }} />
                    <Row style={{ margin: 10 }}>
                        <Col style={{ flex: 2 }}>
                            <Text note style={{ fontWeight: "bold" }}>Önceki Ölçüm</Text>
                        </Col>
                        <Col style={{ flex: 1 }}>
                            <Text note style={{ fontWeight: "bold" }}>{Number.parseFloat(this.state.oncekiOlcum[this.state.selectedValue]).toFixed(2)}</Text>
                        </Col>
                        <Col style={{ flex: 2 }}>
                            <Text note style={{ fontWeight: "bold" }}>{this.state.oncekiOlcum.measurementDateFormatted}</Text>
                        </Col>
                    </Row>
                    <Divider style={{ marginTop: 5, marginBottom: 5, backgroundColor: 'lightgray', height: 1, width: "100%" }} />
                    <Row style={{ margin: 10 }}>
                        <Col style={{ flex: 2 }}>
                            <Text note style={{ fontWeight: "bold" }}>Son Ölçüm</Text>
                        </Col>
                        <Col style={{ flex: 1 }}>
                            <Text note style={{ fontWeight: "bold" }}>{Number.parseFloat(this.state.sonOlcum[this.state.selectedValue]).toFixed(2)}</Text>
                        </Col>
                        <Col style={{ flex: 2 }}>
                            <Text note style={{ fontWeight: "bold" }}>{this.state.sonOlcum.measurementDateFormatted}</Text>
                        </Col>
                    </Row>
                    <Divider style={{ marginTop: 5, marginBottom: 5, backgroundColor: 'lightgray', height: 1, width: "100%" }} />
                    <Row style={{ margin: 10 }}>
                        <Col style={{ flex: 2 }}>
                            <Text note style={{ fontWeight: "bold" }}>Son - Önceki Farkı</Text>
                        </Col>
                        <Col style={{ flex: 1 }}>
                            <Text note style={{ fontWeight: "bold" }}>{(Number.parseFloat(this.state.sonOlcum[this.state.selectedValue]) - Number.parseFloat(this.state.oncekiOlcum[this.state.selectedValue])).toFixed(2)}</Text>
                        </Col>
                        <Col style={{ flex: 1 }} />
                    </Row>
                    <Divider style={{ marginTop: 5, marginBottom: 5, backgroundColor: 'lightgray', height: 1, width: "100%" }} />
                    <Row style={{ margin: 10 }}>
                        <Col style={{ flex: 2 }}>
                            <Text note style={{ fontWeight: "bold" }}>Son - İlk Farkı</Text>
                        </Col>
                        <Col style={{ flex: 1 }}>
                            <Text note style={{ fontWeight: "bold" }}>{(Number.parseFloat(this.state.sonOlcum[this.state.selectedValue]) - Number.parseFloat(this.state.ilkOlcum[this.state.selectedValue])).toFixed(2)}</Text>
                        </Col>
                        <Col style={{ flex: 1 }} />
                    </Row>
                </Grid>
            </View>
        );
    }
}
import React, { Component } from 'react';
import { StyleSheet, View, Platform } from 'react-native';
import { Picker, Item, Icon, Text } from 'native-base';
import ChartView from 'react-native-highcharts';

import WebClient from '../../../helper/webclient';

export default class OlcumTarihcem extends Component {
    constructor() {
        super();

        this.state = {
            selectedValue: "weightKG",
            data: []
        }

        this._createChartConf = this._createChartConf.bind(this);
        this._capitalizeFirstLetter = this._capitalizeFirstLetter.bind(this);
        this._formatDate = this._formatDate.bind(this);
        this.webClient = new WebClient();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isLoaded) {
            this.webClient.get(`Mobile/Measurements/SegmentalGraphic/5`)
                .then(response => {
                    this.setState({ data: response.data.data });
                });
        }
    }

    _capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    _formatDate(date) {
    }

    _createChartConf() {
        var Highcharts = 'Highcharts';
        var conf = {
            chart: {
                type: 'line',
                backgroundColor: 'transparent'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                categories: this.state.data.map(x => x.measurementDate),
                labels: {
                    formatter: function () {
                        var dateTime = new Date(this.value);
                        var result = ("00" + dateTime.getDate()).slice(-2) + "-" + ("00" + (dateTime.getMonth() + 1)).slice(-2) + "-" + dateTime.getFullYear();
                        return result;
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            exporting: {
                enabled: false
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                name: 'Ölçüm Değeriniz',
                data: this.state.data.map(x => Number.parseFloat(x[this.state.selectedValue]))
            }, {
                name: 'İdeal Değer',
                data: this.state.data.map(x => Number.parseFloat(x["ideal" + this._capitalizeFirstLetter(this.state.selectedValue)])),
                color: 'red'
            }]
        };

        return conf;
    }

    render() {
        return (
            <View>
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                    <Text style={{ marginLeft: 10, fontSize: 18, fontWeight: "400", color: "rgb(65,159,221)" }}>Ölçüm Tarihçem</Text>
                    <View style={{
                        flexDirection: "row", alignItems: "center",
                        backgroundColor: "rgb(65,159,221)", borderRadius: 5,
                        marginTop: 10, marginBottom: 10, marginRight: 10, height: 40
                    }}>
                        <Picker info style={{ width: (Platform.OS === 'ios') ? undefined : 150 }}
                            textStyle={{ color: 'white' }}
                            iosHeader="Seçim yapınız"
                            headerBackButtonText="Geri"
                            mode="dropdown"
                            selectedValue={this.state.selectedValue}
                            onValueChange={(value) => { this.setState({ selectedValue: value }) }}>
                            <Item label="Ağırlık(kg)" value="weightKG" />
                            <Item label="Yağ Oranı(%)" value="fatPercent" />
                            <Item label="Yağ(kg)" value="fatKG" />
                            <Item label="Sıvı Oranı(%)" value="tbwPercent" />
                            <Item label="Sıvı(kg)" value="tbwKG" />
                            <Item label="Kas(kg)" value="muscleKG" />
                        </Picker>
                        {
                            Platform.OS == "ios" ?
                                <Icon style={{ marginLeft: -10, marginRight: 10, fontSize: 14, color: "white" }} name="ios-arrow-down" />
                                : null
                        }
                    </View>
                </View>
                <ChartView style={{ height: 250 }} config={this._createChartConf()}></ChartView>
            </View>
        );
    }
}
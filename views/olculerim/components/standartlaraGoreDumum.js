import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'native-base';
import { Divider } from 'react-native-elements';

export default class StandartlaraGoreDurumum extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={{ margin: 5, marginLeft: 10, marginRight: 10 }}>
                <Text style={{ fontSize: 16, fontWeight: "500", color: "rgb(127,127,127)" }}>Standartlara Göre Durumum</Text>
                <View style={{ marginTop: 10 }}>
                    <Text style={{ fontSize: 14, fontWeight: "500", color: "rgb(65,159,221)" }}>Sıvı</Text>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        <Text note>Sıvı Ağırlığı(kg) : {Number.parseInt(this.props.tbwKG)}</Text>
                        <Text note>Sıvı Oranı(%) : {Number.parseInt(this.props.tbwPercent)}</Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <View style={{ flex: 1, alignItems: "flex-start" }}>
                            <Text note style={{ fontSize: 12, fontWeight: "bold", color: "black" }}>{`Düşük < ${Number.parseInt(this.props.tbwPercentLow)}`}</Text>
                            {
                                this.props.tbwPercent < this.props.tbwPercentLow ?
                                    <Divider style={{ marginTop: 5, backgroundColor: 'orange', height: 3, width: "100%" }} /> :
                                    null
                            }
                        </View>
                        <View style={{ flex: 2, alignItems: "center" }}>
                            <Text note style={{ fontSize: 12, fontWeight: "bold", color: "black" }}>{`Normal (${Number.parseInt(this.props.tbwPercentLow)} - ${Number.parseInt(this.props.tbwPercentHigh)})`}</Text>
                            {
                                (this.props.tbwPercentLow < this.props.tbwPercent && this.props.tbwPercent < this.props.tbwPercentHigh) ?
                                    <Divider style={{ marginTop: 5, backgroundColor: 'green', height: 3, width: "70%" }} /> :
                                    null
                            }
                        </View>
                        <View style={{ flex: 1, alignItems: "flex-end" }}>
                            <Text note style={{ fontSize: 12, fontWeight: "bold", color: "black" }}>{`Yüksek > ${Number.parseInt(this.props.tbwPercentHigh)}`}</Text>
                            {
                                this.props.tbwPercent > this.props.tbwPercentHigh ?
                                    <Divider style={{ marginTop: 5, backgroundColor: 'red', height: 3, width: "100%" }} /> :
                                    null
                            }
                        </View>
                    </View>
                </View>
                <View style={{ marginTop: 10 }}>
                    <Text style={{ fontSize: 14, fontWeight: "500", color: "rgb(65,159,221)" }}>Yağ</Text>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        <Text note>Yağ Ağırlığı(kg) : {Number.parseInt(this.props.fatKG)}</Text>
                        <Text note>Yağ Oranı(%) : {Number.parseInt(this.props.fatPercent)}</Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <View style={{ flex: 1, alignItems: "flex-start" }}>
                            <Text note style={{ fontSize: 12, fontWeight: "bold", color: "black" }}>{`Düşük < ${Number.parseInt(this.props.fatPercentLow)}`}</Text>
                            {
                                this.props.fatPercent < this.props.fatPercentLow ?
                                    <Divider style={{ marginTop: 5, backgroundColor: 'orange', height: 3, width: "100%" }} /> :
                                    null
                            }
                        </View>
                        <View style={{ flex: 2, alignItems: "center" }}>
                            <Text note style={{ fontSize: 12, fontWeight: "bold", color: "black" }}>{`Normal (${Number.parseInt(this.props.fatPercentLow)} - ${Number.parseInt(this.props.fatPercentHigh)})`}</Text>
                            {
                                (this.props.fatPercentLow < this.props.fatPercent && this.props.fatPercent < this.props.fatPercentHigh) ?
                                    <Divider style={{ marginTop: 5, backgroundColor: 'green', height: 3, width: "70%" }} /> :
                                    null
                            }
                        </View>
                        <View style={{ flex: 1, alignItems: "flex-end" }}>
                            <Text note style={{ fontSize: 12, fontWeight: "bold", color: "black" }}>{`Yüksek > ${Number.parseInt(this.props.fatPercentHigh)}`}</Text>
                            {
                                this.props.fatPercent > this.props.fatPercentHigh ?
                                    <Divider style={{ marginTop: 5, backgroundColor: 'red', height: 3, width: "100%" }} /> :
                                    null
                            }
                        </View>
                    </View>
                </View>
                <View style={{ marginTop: 10 }}>
                    <Text style={{ fontSize: 14, fontWeight: "500", color: "rgb(65,159,221)" }}>BMI</Text>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        <Text note>Bmi : {Number.parseInt(this.props.bmi)}</Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <View style={{ flex: 1, alignItems: "flex-start" }}>
                            <Text note style={{ fontSize: 12, fontWeight: "bold", color: "black" }}>{`Düşük < ${Number.parseInt(this.props.bmiLow)}`}</Text>
                            {
                                this.props.bmi < this.props.bmiLow ?
                                    <Divider style={{ marginTop: 5, backgroundColor: 'orange', height: 3, width: "100%" }} /> :
                                    null
                            }
                        </View>
                        <View style={{ flex: 2, alignItems: "center" }}>
                            <Text note style={{ fontSize: 12, fontWeight: "bold", color: "black" }}>{`Normal (${Number.parseInt(this.props.bmiLow)} - ${Number.parseInt(this.props.bmiHigh)})`}</Text>
                            {
                                (this.props.bmiLow < this.props.bmi && this.props.bmi < this.props.bmiHigh) ?
                                    <Divider style={{ marginTop: 5, backgroundColor: 'green', height: 3, width: "70%" }} /> :
                                    null
                            }
                        </View>
                        <View style={{ flex: 1, alignItems: "flex-end" }}>
                            <Text note style={{ fontSize: 12, fontWeight: "bold", color: "black" }}>{`Yüksek > ${Number.parseInt(this.props.bmiHigh)}`}</Text>
                            {
                                this.props.bmi > this.props.bmiHigh ?
                                    <Divider style={{ marginTop: 5, backgroundColor: 'red', height: 3, width: "100%" }} /> :
                                    null
                            }
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}
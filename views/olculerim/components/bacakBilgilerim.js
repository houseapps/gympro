import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'native-base';

export default class BacakBilgilerim extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                <View style={{ flexDirection: "column" }}>
                    <Text style={{ margin: 10, fontSize: 14, fontWeight: "500", color: "rgb(127,127,127)" }}>Sol Bacak</Text>
                    <View>
                        <View style={{ flexDirection: "row" }}>
                            <View style={{ alignItems: "flex-start" }}>
                                <Text style={styles.textDetailHeader}>Yağ(%)</Text>
                                <Text style={styles.textDetailHeader}>Yağ(kg)</Text>
                                <Text style={styles.textDetailHeader}>Kas(kg)</Text>
                            </View>
                            <View style={{ alignItems: "flex-end" }}>
                                <Text style={styles.textDetailDetail}>{this.props.leftLegFatPercent}</Text>
                                <Text style={styles.textDetailDetail}>{this.props.leftLegFatKG}</Text>
                                <Text style={styles.textDetailDetail}>{this.props.leftLegMuscleKG}</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{ flexDirection: "column" }}>
                    <Text style={{ margin: 10, fontSize: 14, fontWeight: "500", color: "rgb(127,127,127)" }}>Sağ Bacak</Text>
                    <View>
                        <View style={{ flexDirection: "row" }}>
                            <View style={{ alignItems: "flex-start" }}>
                                <Text style={styles.textDetailHeader}>Yağ(%)</Text>
                                <Text style={styles.textDetailHeader}>Yağ(kg)</Text>
                                <Text style={styles.textDetailHeader}>Kas(kg)</Text>
                            </View>
                            <View style={{ alignItems: "flex-end" }}>
                                <Text style={styles.textDetailDetail}>{this.props.rightLegFatPercent}</Text>
                                <Text style={styles.textDetailDetail}>{this.props.rightLegFatKG}</Text>
                                <Text style={styles.textDetailDetail}>{this.props.rightLegMuscleKG}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    textDetailHeader: {
        marginLeft: 10,
        fontSize: 14,
        fontWeight: "bold",
        color: "rgb(65,159,221)"
    },
    textDetailDetail: {
        marginLeft: 10,
        marginRight: 10,
        fontSize: 14,
        fontWeight: "300",
        color: "rgb(127,127,127)"
    }
});
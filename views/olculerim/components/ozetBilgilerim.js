import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'native-base';

export default class OzetBilgilerim extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <View>
                <Text style={{ margin: 10, fontSize: 18, fontWeight: "500", color: "rgb(127,127,127)" }}>Özet Bilgiler</Text>
                <View style={{ flex: 2, flexDirection: "row" }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ flex: 1, alignItems: "flex-start" }}>
                            <Text style={styles.textDetailHeader}>Vücut Tipi</Text>
                            <Text style={styles.textDetailHeader}>Yaş</Text>
                            <Text style={styles.textDetailHeader}>Boy</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: "flex-end" }}>
                            <Text style={styles.textDetailDetail}>{this.props.bodyTypeName}</Text>
                            <Text style={styles.textDetailDetail}>{this.props.age}</Text>
                            <Text style={styles.textDetailDetail}>{this.props.heightCM}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ flex: 1, alignItems: "flex-start" }}>
                            <Text style={styles.textDetailHeader}>Kilo</Text>
                            <Text style={styles.textDetailHeader}>BMI</Text>
                            <Text style={styles.textDetailHeader}>Yağ(%)</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: "flex-end" }}>
                            <Text style={styles.textDetailDetail}>{this.props.weightKG}</Text>
                            <Text style={styles.textDetailDetail}>{this.props.bmi}</Text>
                            <Text style={styles.textDetailDetail}>{this.props.fatPercent}</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    textDetailHeader: {
        marginLeft: 15,
        fontSize: 14,
        fontWeight: "bold",
        color: "rgb(65,159,221)"
    },
    textDetailDetail: {
        marginRight: 10,
        fontSize: 14,
        fontWeight: "300",
        color: "rgb(127,127,127)"
    }
});
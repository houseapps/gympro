import React, { Component } from 'react';
import { View, ScrollView, ImageBackground, StyleSheet, Platform, Dimensions } from 'react-native';
import { Tab, Tabs, TabHeading, Text, Button, Picker, Item, Icon } from 'native-base';
import { Divider } from 'react-native-elements';
import _ from 'lodash';

import HeaderBar from '../HeaderBar'
import OzetBilgilerim from './components/ozetBilgilerim';
import KolBilgilerim from './components/kolBilgilerim';
import GovdeBilgilerim from './components/govdeBilgilerim';
import BacakBilgilerim from './components/bacakBilgilerim';
import StandartlaraGoreDurumum from './components/standartlaraGoreDumum';
import Olculerim from './components/olculerim';
import OlcumTarihcem from './components/olcumTarihcem';

import WebClient from '../../helper/webclient';

const title = "Ölçümlerim";

export default class Index extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor(props) {
        super(props);
        this.state = {
            measurements: [],
            selectedMeasurement: { gender: 1 },
            selectedDateIndex: 0,
            dates: [],
            isLoaded: false
        };

        this.webClient = new WebClient();
    }

    componentWillMount() {
        setTimeout(() => {
            this.webClient.get(`Mobile/Measurements`)
                .then(response => {
                    var dates = response.data.data.map(x => x.measurementDateFormatted);
                    this.setState({
                        measurements: response.data.data,
                        selectedMeasurement: response.data.data[0],
                        selectedDateIndex: 0,
                        dates: dates,
                        isLoaded: true
                    });
                }).catch(error => this.props.navigation.goBack());
            }, 500);
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%", flexDirection: "column" }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                    <Text style={{ marginLeft: 10, fontSize: 18, fontWeight: "400", color: "rgb(65,159,221)" }}>Tüm Ölçümlerim</Text>
                    <View style={{
                        flexDirection: "row", alignItems: "center",
                        backgroundColor: "rgb(65,159,221)", borderRadius: 5,
                        marginTop: 10, marginBottom: 10, marginRight: 10, height: 40
                    }}>
                        <Picker info
                            style={{ margin: 7, marginRight: 10, width: (Platform.OS === 'ios') ? undefined : 200 }}
                            textStyle={{ color: 'white' }}
                            iosHeader="Seçim yapınız"
                            headerBackButtonText="Geri"
                            mode="dropdown"
                            selectedValue={this.state.selectedDateIndex}
                            onValueChange={(index) => { this.setState({ selectedDateIndex: index, selectedMeasurement: this.state.measurements[index] }) }}>
                            {
                                this.state.dates.map((date, index) => {
                                    return <Item key={index} label={date} value={index} />
                                })
                            }
                        </Picker>
                        {
                            Platform.OS == "ios" ?
                                <Icon style={{ marginLeft: -10, marginRight: 10, fontSize: 14, color: "white" }} name="ios-arrow-down" />
                                : null
                        }
                    </View>
                </View>
                <Divider style={{ backgroundColor: 'rgb(65,159,221)', height: 3, width: "100%" }} />
                <ScrollView>
                    <OzetBilgilerim {...this.state.selectedMeasurement} />
                    <Divider style={{ marginTop: 5, marginBottom: 3, backgroundColor: 'rgb(127,127,127)', height: 1, width: "100%" }} />
                    <View style={{ height: 380, flexDirection: "column", justifyContent: "space-around" }}>
                        <KolBilgilerim {...this.state.selectedMeasurement} isLoaded={this.state.isLoaded} />
                        <GovdeBilgilerim {...this.state.selectedMeasurement} isLoaded={this.state.isLoaded} />
                        <BacakBilgilerim {...this.state.selectedMeasurement} isLoaded={this.state.isLoaded} />
                        {
                            this.state.selectedMeasurement.gender == 1 ?
                                <ImageBackground source={require("../../assets/backgroudMan.png")} style={{ marginTop: -20, width: 130, height: 380, position: "absolute", left: Dimensions.get('window').width / 2 - 55 }} /> :
                                <ImageBackground source={require("../../assets/backgroundWoman.png")} style={{ marginTop: -20, width: 130, height: 380, position: "absolute", left: Dimensions.get('window').width / 2 - 55 }} />
                        }
                    </View>
                    <Divider style={{ marginTop: 5, marginBottom: 5, backgroundColor: 'rgb(127,127,127)', height: 1, width: "100%" }} />
                    <StandartlaraGoreDurumum {...this.state.selectedMeasurement} isLoaded={this.state.isLoaded} />
                    <Divider style={{ marginTop: 5, marginBottom: 5, backgroundColor: 'rgb(127,127,127)', height: 1, width: "100%" }} />
                    <Olculerim isLoaded={this.state.isLoaded} />
                    <Divider style={{ marginTop: 5, marginBottom: 5, backgroundColor: 'rgb(127,127,127)', height: 1, width: "100%" }} />
                    <OlcumTarihcem isLoaded={this.state.isLoaded} />
                </ScrollView>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    textDetailHeader: {
        marginLeft: 20,
        fontSize: 14,
        fontWeight: "bold",
        color: "rgb(65,159,221)"
    },
    textDetailDetail: {
        marginRight: 30,
        fontSize: 14,
        fontWeight: "300",
        color: "rgb(127,127,127)"
    }
});
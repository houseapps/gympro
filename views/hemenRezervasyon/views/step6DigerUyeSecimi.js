import React, { Component } from 'react';
import { ScrollView, View } from 'react-native';
import { Item, Label, Input, Button, Text } from 'native-base';

import WebClient from '../../../helper/webclient';

export default class Step2 extends Component {
    constructor(props) {
        super(props);
        this.webClient = new WebClient();

        this._onItemSelect = this._onItemSelect.bind(this);
        this._getChildObjects = this._getChildObjects.bind(this);

        this.state = {
            items: this._getChildObjects()
        }
    }

    _getChildObjects() {
        let childObjects = [];
        for (let index = 0; index < this.props.saat.serviceNumberOfMember; index++) {
            childObjects.push({ memberID: -1, ad: "", soyad: "", cepNo: "" });
            // childObjects.push({ memberID: -1, ad: "TEST1", soyad: "TEST", cepNo: "1111111111" });
        }

        return childObjects;
    }

    _changeText(index, key, value) {
        var obj = this.state.items[index];
        obj[key] = value;
        this.setState({ items: this.state.items });
    }

    _onItemSelect() {
        let promiseList = [];
        for (let index = 0; index < this.state.items.length; index++) {
            const element = this.state.items[index];
            let promise = this.webClient.post(`Mobile/IndividualReservations/MemberSearch`, {
                CategoryID: this.props.kategori.categoryID,
                Name: element.ad,
                Surname: element.soyad,
                Phone: element.cepNo
            })
                .then(response => {
                    element.memberID = response.data.data.memberID
                });
            promiseList.push(promise);
        }

        Promise.all(promiseList).then(() =>
            this.setState({ items: this.state.items }, this.props.onNext(this.state.items)));
    }

    render() {
        return (
            <ScrollView>
                {
                    this.state.items.map((item, index) =>
                        <View key={index} style={{ flexDirection: "row", alignItems: "center" }}>
                            <Label style={{ marginLeft: 2, marginTop: -4 }}>{index + 1}.</Label>
                            <Item style={{ flex: 1 }} >
                                <Input placeholder="Adı" value={item.ad} onChangeText={(text) => this._changeText(index, "ad", text)} />
                            </Item>
                            <Item style={{ flex: 1 }} >
                                <Input placeholder="Soyadı" value={item.soyad} onChangeText={(text) => this._changeText(index, "soyad", text)} />
                            </Item>
                            <Item style={{ flex: 1 }} >
                                <Input placeholder="Cep No" value={item.cepNo} onChangeText={(text) => this._changeText(index, "cepNo", text)} />
                            </Item>
                        </View>
                    )
                }
                <Button full success style={{ marginTop: 10 }} onPress={() => this._onItemSelect()}>
                    <Text>DEVAM ET</Text>
                </Button>
            </ScrollView>
        );
    }
}

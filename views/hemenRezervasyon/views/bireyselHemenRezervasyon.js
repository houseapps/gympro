import React, { Component } from 'react';
import { View, Dimensions, ScrollView } from 'react-native';
import {
    Container, Header, Content, Tab, Tabs, TabHeading, Text,
    List, ListItem, Left, Body, Right, Button,
    Footer, FooterTab
} from 'native-base';
import { Calendar, CalendarList } from 'react-native-calendars';

import HeaderBar from '../../HeaderBar'
import WebClient from '../../../helper/webclient';
import BireyselRezervasyonItem from '../components/bireyselRezervasyonItem';

import Step1 from './step1KategoriSecimi';
import Step2 from './step2HizmetSecimi';
import Step3 from './step3KaynakSecimi';
import Step4 from './step4TarihSecimi';
import Step5 from './step5SaatSecimi';
import Step6 from './step6DigerUyeSecimi';
import Step7 from './step7EgitmenSecimi';
import Step8 from './step8PaketSecimi';

const title = "Bireysel Rezervasyon";

export default class BireyselHemenRezervasyon extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor() {
        super();
        this.webClient = new WebClient();

        this.state = {
            activeItem: 0,
            selectedKategori: undefined,
            selectedHizmet: undefined,
            isShowKaynak: true,
            selectedKaynak: undefined,
            selectedTarih: undefined,
            selectedSaat: undefined,
            isShowDigerUyeSecimi: true,
            isShowEgitmenSecimi: true,
            isShowPaketSecimi: true,
            selectedDigerUyeler: [],
            selectedEgitmenler: [],
            selectedPaket: undefined
        }

        this._getStatus = this._getStatus.bind(this);
        this._getHeader = this._getHeader.bind(this);
        this._finishBireyselRezervasyon = this._finishBireyselRezervasyon.bind(this);
    }

    _getStatus(key, isShow = true) {
        if (!isShow) {
            return "nan";
        }

        if (this.state.activeItem == key) {
            return "active";
        }

        if (this.state.activeItem < key) {
            return "nan";
        }

        return "submit";
    }

    _getHeader(key, isShow = true) {
        switch (key) {
            case 0:
                return this._getStatus(key, isShow) == "submit" ? this.state.selectedKategori.categoryName : "Kategori Seçin";
            case 1:
                return this._getStatus(key, isShow) == "submit" ? this.state.selectedHizmet.serviceName : "Hizmet Seçin";
            case 2:
                return this._getStatus(key, isShow) == "submit" ? this.state.selectedKaynak.resourceName : "Kaynak Seçin";
            case 3:
                return this._getStatus(key, isShow) == "submit" ? this.state.selectedTarih.day + "-" + ("00" + this.state.selectedTarih.month).slice(-2) + "-" + this.state.selectedTarih.year : "Tarih Seçin";
            case 4:
                return this._getStatus(key, isShow) == "submit" ? ("00" + this.state.selectedSaat.timeHour).slice(-2) + ":" + ("00" + this.state.selectedSaat.timeMinute).slice(-2) : "Saat Seçin";
            case 5:
                return "Diğer Üyeleri Seçin";
            case 6:
                return "Eğitmen Seçin";
            case 7:
                return this._getStatus(key, isShow) == "submit" ? this.state.selectedPaket.packageName : "Paket Seçin";
        }
    }

    _finishBireyselRezervasyon() {
        let resource = 0;
        
        if (this.state.isShowKaynak) {
            resource = this.state.selectedKaynak == undefined ? 0 : this.state.selectedKaynak.resourceID;
        } else {
            resource = this.state.selectedSaat.resourceID;
        }
        
        this.webClient.get(`Mobile/IndividualReservations/Rules`)
            .then(response => {
                this.props.navigation.navigate("KullanimKosullari", {
                    accept: () => this.webClient.post(`Mobile/IndividualReservations`,
                        {
                            CategoryID: this.state.selectedKategori.categoryID,
                            ServiceID: this.state.selectedHizmet.serviceID,
                            ResourceID: resource,
                            Date: this.state.selectedTarih.dateString,
                            Hour: this.state.selectedSaat.timeHour,
                            Minute: this.state.selectedSaat.timeMinute,
                            ApprovalPackageID: this.state.selectedPaket == undefined ? 0 : this.state.selectedPaket.packageID,
                            OtherMemberIDs: this.state.selectedDigerUyeler == undefined ? [] : this.state.selectedDigerUyeler.map(x => x.memberID),
                            TrainerIDs: this.state.selectedEgitmenler == undefined ? [] : this.state.selectedEgitmenler.map(x => x.trainerID)
                        })
                        .then(response => {
                            this.props.navigation.navigate("Bilgilendirme");
                        }),
                    reject: () => { },
                    doc: response.data.data
                })
                this.setState({ items: response.data.data });
            });
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%" }}>
                <ScrollView style={{ flex: 1, backgroundColor: "white" }}>
                    <BireyselRezervasyonItem index={1} title={this._getHeader(0)} status={this._getStatus(0)}
                        onPress={() => this.setState({ activeItem: 0 })}>
                        <Step1
                            onNext={(kategori) => this.setState({ activeItem: 1, selectedKategori: kategori })} />
                    </BireyselRezervasyonItem>

                    <BireyselRezervasyonItem index={2} title={this._getHeader(1)} status={this._getStatus(1)}
                        onPress={() => { this.setState({ activeItem: 1 }) }}>
                        <Step2 kategori={this.state.selectedKategori}
                            onNext={(hizmet) => this.setState({ activeItem: hizmet.isSelectResource ? 2 : 3, isShowKaynak: hizmet.isSelectResource, selectedHizmet: hizmet })} />
                    </BireyselRezervasyonItem>

                    <BireyselRezervasyonItem index={3} title={this._getHeader(2, this.state.isShowKaynak)} status={this._getStatus(2, this.state.isShowKaynak)}
                        onPress={() => { this.setState({ activeItem: 2 }) }}>
                        <Step3 kategori={this.state.selectedKategori} hizmet={this.state.selectedHizmet}
                            onNext={(kaynak) => this.setState({ activeItem: 3, selectedKaynak: kaynak })} />
                    </BireyselRezervasyonItem>

                    <BireyselRezervasyonItem index={4} title={this._getHeader(3)} status={this._getStatus(3)}
                        onPress={() => { this.setState({ activeItem: 3 }) }}>
                        <Step4 kategori={this.state.selectedKategori} hizmet={this.state.selectedHizmet} kaynak={this.state.selectedKaynak}
                            onNext={(tarih) => this.setState({ activeItem: 4, selectedTarih: tarih })} />
                    </BireyselRezervasyonItem>

                    <BireyselRezervasyonItem index={5} title={this._getHeader(4)} status={this._getStatus(4)}
                        onPress={() => { this.setState({ activeItem: 4 }) }}>
                        <Step5 kategori={this.state.selectedKategori} hizmet={this.state.selectedHizmet} kaynak={this.state.selectedKaynak} tarih={this.state.selectedTarih}
                            onNext={(saat) => this.setState(
                                {
                                    activeItem: saat.serviceNumberOfMember > 1 ? 5 : (saat.serviceNumberOfTrainer > 0 ? 6 : (saat.isSelectPacket ? 7 : 8)),
                                    isShowDigerUyeSecimi: saat.serviceNumberOfMember > 1,
                                    isShowEgitmenSecimi: saat.serviceNumberOfTrainer > 0,
                                    isShowPaketSecimi: saat.isSelectPacket,
                                    selectedSaat: saat
                                })} />
                    </BireyselRezervasyonItem>

                    <BireyselRezervasyonItem index={6} title={this._getHeader(5, this.state.isShowDigerUyeSecimi)} status={this._getStatus(5, this.state.isShowDigerUyeSecimi)}
                        onPress={() => { this.setState({ activeItem: 5 }) }}>
                        <Step6 kategori={this.state.selectedKategori} hizmet={this.state.selectedHizmet} kaynak={this.state.selectedKaynak} tarih={this.state.selectedTarih} saat={this.state.selectedSaat}
                            onNext={(secilenUyeler) => this.setState(
                                {
                                    activeItem: this.state.selectedSaat.serviceNumberOfTrainer > 0 ? 6 : (this.state.selectedSaat.isSelectPacket ? 7 : 8),
                                    selectedDigerUyeler: secilenUyeler
                                })} />
                    </BireyselRezervasyonItem>

                    <BireyselRezervasyonItem index={7} title={this._getHeader(6, this.state.isShowEgitmenSecimi)} status={this._getStatus(6, this.state.isShowEgitmenSecimi)}
                        onPress={() => { this.setState({ activeItem: 6 }) }}>
                        <Step7 kategori={this.state.selectedKategori} hizmet={this.state.selectedHizmet} kaynak={this.state.selectedKaynak} tarih={this.state.selectedTarih} saat={this.state.selectedSaat}
                            onNext={(secilenEgitmenler) => this.setState(
                                {
                                    activeItem: this.state.selectedSaat.isSelectPacket ? 7 : 8,
                                    selectedEgitmenler: secilenEgitmenler
                                })} />
                    </BireyselRezervasyonItem>

                    <BireyselRezervasyonItem index={8} title={this._getHeader(7, this.state.isShowPaketSecimi)} status={this._getStatus(7, this.state.isShowPaketSecimi)}
                        onPress={() => { this.setState({ activeItem: 7 }) }}>
                        <Step8 kategori={this.state.selectedKategori} hizmet={this.state.selectedHizmet} kaynak={this.state.selectedKaynak} tarih={this.state.selectedTarih} saat={this.state.selectedSaat}
                            onNext={(paket) => this.setState(
                                {
                                    activeItem: 8,
                                    selectedPaket: paket
                                })} />
                    </BireyselRezervasyonItem>
                </ScrollView>
                {
                    this.state.activeItem == 0 ?
                        null :
                        <View style={{ flexDirection: "row" }}>
                            <Button full danger style={{ width: this.state.activeItem == 8 ? "50%" : "100%" }}
                                onPress={() => { this.setState({ activeItem: 0 }) }}>
                                <Text>SEÇİMİMİ SIFIRLA</Text>
                            </Button>
                            {
                                this.state.activeItem == 8 ?
                                    <Button full success style={{ width: "50%" }}
                                        onPress={this._finishBireyselRezervasyon}>
                                        <Text>DEVAM ET</Text>
                                    </Button> :
                                    null
                            }
                        </View>
                }
            </View>
        );
    }
}
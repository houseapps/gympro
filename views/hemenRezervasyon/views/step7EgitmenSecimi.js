import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { List, ListItem, Text, Button, Picker, Item } from 'native-base';

import WebClient from '../../../helper/webclient';

export default class Step7 extends Component {
    constructor(props) {
        super(props);
        this.webClient = new WebClient();
        this._onItemSelect = this._onItemSelect.bind(this);
        this._getChildObjects = this._getChildObjects.bind(this);
        this.state = {
            selectedItems: this._getChildObjects(),
            items: []
        }
    }

    componentDidMount() {
        this.webClient.get(`Mobile/IndividualReservations/Trainers/${this.props.hizmet.serviceID}/${this.props.tarih.dateString}/${this.props.saat.timeHour}/${this.props.saat.timeMinute}`)
            .then(response => {
                this.setState({ items: response.data.data });
            });
    }

    _getChildObjects() {
        let childObjects = [];
        for (let index = 0; index < this.props.saat.serviceNumberOfTrainer; index++) {
            childObjects.push({ trainerID: -1, trainerName: "" });
        }

        return childObjects;
    }

    _onItemSelect() {
        this.props.onNext(this.state.selectedItems);
    }

    render() {
        return (
            <ScrollView>
                {
                    this.state.selectedItems.map((item, index) =>
                        <Picker key={index}
                            textStyle={{ color: 'black' }}
                            iosHeader="Seçim yapınız"
                            headerBackButtonText="Geri"
                            mode="dropdown"
                            placeholder="Eğitmen seçiniz"
                            selectedValue={this.state.selectedItems[index]}
                            onValueChange={(sender) => {
                                this.state.selectedItems[index] = sender;
                                this.setState({ selectedItems: this.state.selectedItems });
                            }}>
                            {
                                this.state.items.map((item, index) => <Item key={item.trainerID} value={item} label={item.trainerName} />)
                            }
                        </Picker>
                    )
                }
                <Button full success style={{ marginTop: 10 }} onPress={() => this._onItemSelect()}>
                    <Text>DEVAM ET</Text>
                </Button>
            </ScrollView>
        );
    }
}

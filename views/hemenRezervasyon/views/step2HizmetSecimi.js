import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { List, ListItem, Text } from 'native-base';

import WebClient from '../../../helper/webclient';

export default class Step2 extends Component {
    constructor(props) {
        super(props);
        this.webClient = new WebClient();
        this._onItemSelect = this._onItemSelect.bind(this);
        this.state = {
            selectedItem: undefined,
            items: []
        }
    }

    componentDidMount() {
        this.webClient.get(`Mobile/IndividualReservations/Services/${this.props.kategori.categoryID}`)
            .then(response => {
                this.setState({
                    items: response.data.data
                });
            });
    }

    _onItemSelect(item) {
        this.setState({ selectedItem: item }, () => this.props.onNext(item));
    }

    render() {
        return (
            <ScrollView>
                <List>
                    {
                        this.state.items.map(item =>
                            <ListItem key={item.serviceID} onPress={() => this._onItemSelect(item)}>
                                <Text>{item.serviceName}</Text>
                            </ListItem>
                        )
                    }
                </List>
            </ScrollView>
        );
    }
}

import React, { Component } from 'react';
import { ScrollView, View } from 'react-native';
import { List, ListItem, Body, Text } from 'native-base';

import WebClient from '../../../helper/webclient';

export default class Step8 extends Component {
    constructor(props) {
        super(props);
        this.webClient = new WebClient();
        this._onItemSelect = this._onItemSelect.bind(this);
        this.state = {
            selectedItem: undefined,
            items: []
        }
    }

    componentDidMount() {
        this.webClient.get(`Mobile/IndividualReservations/Packages/${this.props.hizmet.serviceID}`)
            .then(response => {
                this.setState({ items: response.data.data });
            });
    }

    _onItemSelect(item) {
        this.setState({ selectedItem: item }, () => this.props.onNext(item));
    }

    render() {
        return (
            <ScrollView>
                <List>
                    {
                        this.state.items.map(item =>
                            <ListItem key={item.packageID} onPress={() => this._onItemSelect(item)}>
                                <Body>
                                    <Text style={{ margin: 2, fontWeight: "bold" }}>{item.packageName}</Text>
                                    <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>
                                        <Text style={{ margin: 2, fontSize: 14 }}>Kalan: {item.remainingCredit}</Text>
                                        <Text style={{ margin: 2, fontSize: 14 }}>Son kullanım: {new Date(item.endDate).toLocaleDateString("tr-TR")}</Text>
                                    </View>
                                </Body>
                            </ListItem>
                        )
                    }
                </List>
            </ScrollView>
        );
    }
}

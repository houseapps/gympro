import React, { Component } from 'react';
import { View, Dimensions, ScrollView } from 'react-native';
import { List, ListItem, Text } from 'native-base';
import { Calendar, CalendarList, LocaleConfig } from 'react-native-calendars';

import WebClient from '../../../helper/webclient';

LocaleConfig.locales['tr'] = {
    monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
    monthNamesShort: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
    dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
    dayNamesShort: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi']
};

LocaleConfig.defaultLocale = 'tr';

export default class Step4 extends Component {
    constructor(props) {
        super(props);
        this.webClient = new WebClient();
        this._onItemSelect = this._onItemSelect.bind(this);
        this.state = {
            selectedItem: undefined
        }
    }

    _onItemSelect(item) {
        this.setState({ selectedItem: item }, () => this.props.onNext(item));
    }

    render() {
        return (
            <View style={{ width: Dimensions.get('window').width, height: 400 }}>
                <Calendar current={this.state.selectedTarih}
                    onDayPress={(day) => this._onItemSelect(day)} />
            </View>
        );
    }
}

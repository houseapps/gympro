import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { List, ListItem, Text } from 'native-base';

import WebClient from '../../../helper/webclient';
import * as AppActions from '../../../redux/actions/app'

export default class Step2 extends Component {
    constructor(props) {
        super(props);
        this.webClient = new WebClient();
        this._onItemSelect = this._onItemSelect.bind(this);
        this.state = {
            selectedItem: undefined,
            items: []
        }
    }

    componentDidMount() {
        this.webClient.get(`Mobile/IndividualReservations/AvailableTimes/${this.props.kategori.categoryID}/${this.props.hizmet.serviceID}/${this.props.kaynak == undefined ? 0 : this.props.kaynak.resourceID}/${this.props.tarih.dateString}`)
            .then(response => {
                this.setState({
                    items: response.data.data
                });
            });
    }

    _onItemSelect(item) {
        this.webClient.get(`Mobile/IndividualReservations/Time/${this.props.kategori.categoryID}/${this.props.hizmet.serviceID}/${this.props.kaynak == undefined ? 0 : this.props.kaynak.resourceID}/${this.props.tarih.dateString}/${item.timeHour}/${item.timeMinute}`)
            .then(response => {
                if (response.data.data.userMessage != "") {
                    AppActions.alert("Bilgi", response.data.data.userMessage);
                }
                this.setState({ selectedItem: item }, () => this.props.onNext({ ...item, ...response.data.data }));
            });
    }

    render() {
        return (
            <ScrollView>
                <List>
                    {
                        this.state.items.map((item, index) =>
                            <ListItem key={index} onPress={() => this._onItemSelect({ timeHour: item.timeHour, timeMinute: item.timeMinute, resourceID: item.resourceID })}>
                                <Text>{("00" + item.timeHour).slice(-2) + ":" + ("00" + item.timeMinute).slice(-2)}</Text>
                            </ListItem>
                        )
                    }
                </List>
            </ScrollView>
        );
    }
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, ImageBackground, TouchableOpacity, Dimensions } from 'react-native';
import { ListItem, Left, Body, Right, Thumbnail, Text, Button } from 'native-base';

const statusColor = {
    active: "rgb(107,174,216)",
    submit: "rgb(191,212,68)"
}

const statusIcon = {
    active: <ImageBackground source={require("../../../assets/beyazAltOk.png")}
        style={{ marginRight: 25, width: 25, height: 25 }} />,
    submit: <ImageBackground source={require("../../../assets/beyazOk.png")}
        style={{ marginRight: 25, width: 25, height: 25 }} />
}

export default class BireyselRezervasyonItem extends Component {
    static defaultProps = {
        title: "Kategori Seçin",
        status: "active",
        onPress: () => { }
    }

    render() {
        if (this.props.status === "nan") {
            return null;
        }

        return (
            <View>
                <TouchableOpacity onPress={this.props.onPress}
                    style={{
                        height: 40,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        backgroundColor: this.props.status === "active" ?
                            statusColor.active :
                            statusColor.submit
                    }}>
                    <Text style={{ marginLeft: 10, color: "white", fontWeight: "600" }}>
                        {this.props.title}
                    </Text>
                    {
                        this.props.status === "active" ?
                            statusIcon.active :
                            statusIcon.submit
                    }
                </TouchableOpacity>
                {
                    this.props.status === "active" ?
                        this.props.children :
                        null
                }
            </View>
        );
    }
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, ImageBackground, TouchableOpacity } from 'react-native';
import { ListItem, Left, Body, Right, Thumbnail, Text, Button } from 'native-base';

export default class HemenRezervasyonTypeItem extends Component {
    static defaultProps = {
        title: "Bireysel Rezervasyon",
        onPress: () => { }
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress}
                style={{
                    height: 60,
                    marginTop: 10,
                    marginLeft: 5,
                    marginRight: 5,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    backgroundColor: "rgb(238,143,76)"
                }}>
                <Text style={{ marginLeft: 10, color: "white", fontWeight: "600" }}>
                    {this.props.title}
                </Text>
                <ImageBackground source={require("../../../assets/beyazSagOk.png")}
                    style={{ marginRight: 20, width: 30, height: 20 }} />
            </TouchableOpacity>
        );
    }
}

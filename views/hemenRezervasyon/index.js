import React, { Component } from 'react';
import { View } from 'react-native';
import { Container, Header, Content, Tab, Tabs, TabHeading, Text } from 'native-base';

import HeaderBar from '../HeaderBar'
import HemenRezervasyonTypeItem from './components/hemenRezervasyonTypeItem';

const title = "Hemen Rezervasyon";

export default class Index extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor() {
        super();
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%", }}>
                <HemenRezervasyonTypeItem title="Bireysel Rezervasyon"
                    onPress={() => this.props.navigation.navigate("BireyselHemenRezarvasyonlarim")} />
                <HemenRezervasyonTypeItem title="Stüdyo Rezervasyonu"
                    onPress={() => this.props.navigation.navigate("StudyoHemenRezervasyonlarim")} />
            </View>
        );
    }
}
import React from "react";
import { ImageBackground, View } from "react-native";
import { Footer, FooterTab, Button } from "native-base";
import { NavigationActions } from 'react-navigation';
import { Divider } from 'react-native-elements';

export default class FooterBar extends React.Component {
    constructor() {
        super();
        this._navigate = this._navigate.bind(this);
    }

    _navigate(path) {
        this.props.navigation.dispatch({
            type: NavigationActions.NAVIGATE,
            routeName: path,
            action: {
                type: NavigationActions.RESET,
                index: 0,
                actions: [{ type: NavigationActions.NAVIGATE, routeName: path }]
            }
        });
    }

    _navigateN(path) {
        const resetAction = NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: "Main" })
          ]
        });
        this.props.navigation.dispatch(resetAction);
    }    

    _navigateNS(path) {
        const resetAction = NavigationActions.reset({
          index: 1,
          actions: [
            NavigationActions.navigate({ routeName: "Main" }),
            NavigationActions.navigate({ routeName: path })
          ]
        });
        this.props.navigation.dispatch(resetAction);
    }     

    render() {
        return (
            <Footer style={{ backgroundColor: "rgb(91, 91, 91)" }}>
                <FooterTab style={{ backgroundColor: "rgb(91, 91, 91)" }}>
                    <Button full
                        onPress={() => this.props.navigation.navigate("DrawerOpen")}>
                        <ImageBackground source={require("../assets/footerSol.png")}
                            style={{ width: 30, height: 30 }} />
                    </Button>
                    <Divider style={{ backgroundColor: 'rgb(158,158,158)', height: 45, width: 1, marginTop: 5 }} />
                    <Button full
                        onPress={() => this._navigateN("Main")}>
                        <ImageBackground source={require("../assets/footerOrta.png")}
                            style={{ width: 30, height: 30 }} />
                    </Button>
                    <Divider style={{ backgroundColor: 'rgb(158,158,158)', height: 45, width: 1, marginTop: 5 }} />
                    <Button full
                        onPress={() => this._navigateNS("Info")}>
                        <ImageBackground source={require("../assets/footerSag.png")}
                            style={{ width: 30, height: 30 }} />
                    </Button>
                </FooterTab>
            </Footer>
        );
    }
}
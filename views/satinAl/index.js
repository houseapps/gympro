import React, { Component } from 'react';
import { View, ImageBackground, TouchableOpacity, ScrollView } from 'react-native';
import {
    Container, Header, Content, Tab,
    Tabs, TabHeading, Text, List, ListItem,
    Left, Right, Body
} from 'native-base';

import HeaderBar from '../HeaderBar'
import WebClient from '../../helper/webclient'

const title = "Satın Al";

export default class Index extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor(props) {
        super(props);
        this.webClient = new WebClient();

        this.state = {
            packages: []
        }
    }

    componentWillMount() {
        this.webClient.get(`Mobile/Sales/Packages`)
            .then(response => {
                this.setState({ packages: response.data.data });
            });
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%", }}>
                <View style={{
                    height: 40,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    backgroundColor: "rgb(191,212,68)"
                }}>
                    <Text style={{ marginLeft: 10, color: "white", fontWeight: "600" }}>
                        Paket Satışı
                    </Text>
                    <ImageBackground source={require("../../assets/beyazAltOk.png")}
                        style={{ marginRight: 25, width: 25, height: 25 }} />
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <List>
                        {
                            this.state.packages.map((item, index) =>
                                <ListItem key={index} style={{ marginLeft: -5 }}
                                    onPress={() => this.props.navigation.navigate("SatinAlSummary", item)}>
                                    <Body>
                                        <Text style={{ margin: 2, fontWeight: "bold" }}>{item.packageName}</Text>
                                        <Text style={{ margin: 2, fontSize: 14 }}>{item.description}</Text>
                                    </Body>
                                    <Right>
                                        <Text note>{item.priceFormatted}</Text>
                                    </Right>
                                </ListItem>
                            )
                        }
                    </List>
                </ScrollView>
            </View>
        );
    }
}
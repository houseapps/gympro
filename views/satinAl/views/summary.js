import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Tab, Tabs, TabHeading, Text, CheckBox, Button } from 'native-base';
import { Divider } from 'react-native-elements';

import * as Actions from '../../../redux/actions/app';
import WebClient from '../../../helper/webclient';
import HeaderBar from '../../HeaderBar';


const title = "Satın Al";

export default class Summary extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor(props) {
        super(props);
        this.webClient = new WebClient();

        this.state = {
            rule: {
                accept: () => { this.setState({ isAccepted: true }) },
                reject: () => { this.setState({ isAccepted: false }) },
                doc: ""
            },
            isAccepted: false,
            package: this.props.navigation.state.params
        }

        this._satinAl = this._satinAl.bind(this);
    }

    componentWillMount() {
        this.webClient.get(`Mobile/Sales/Rules`)
            .then(response => {
                this.setState({
                    rule: {
                        accept: () => { this.setState({ isAccepted: true }) },
                        reject: () => { this.setState({ isAccepted: false }) },
                        doc: response.data.data
                    }
                });
            });
    }

    _satinAl() {
        if (this.state.isAccepted) {
            this.webClient.post(`Mobile/Sales/Package`, {
                PackageID: this.state.package.packageID,
                SalesDate: new Date(),
                PaymentDate: new Date(),
                Price: this.state.package.price,
                RefNo: "",
                ProductSaleID: 0,
                VirtualPos: "ECUZDAN",
                IpAddress: "127.0.0.1"
            })
                .then(response => {
                    this.props.navigation.navigate("SatinAlBilgilendirme", this.state.package.eWalletAfter);
                });
        }
        else {
            Actions.alert("Hata", "Kullanım koşullarını kabul etmelisiniz.");
        }
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%", }}>
                <View style={{ flex: 1 }}>
                    <View style={{ justifyContent: "center", alignItems: "center" }}>
                        <Text>Paket Fiyatı</Text>
                        <Text style={{ margin: 5, fontSize: 22, color: "rgb(65,159,221)" }}>{this.state.package.priceFormatted}</Text>

                        <Text>E-Cüzdan Bakiyeniz</Text>
                        <Text style={{ margin: 5, fontSize: 22, color: "rgb(238,130,48)" }}>{this.state.package.eWalletBefore}</Text>

                        <Text>Alım sonrası kalan e-cüzdan bakiyesi</Text>
                        <Text style={{ margin: 5, fontSize: 22, color: "rgb(238,130,48)" }}>{this.state.package.eWalletAfter}</Text>

                        <Text style={{ margin: 5, marginTop: 20 }}>{this.state.package.packageName}</Text>
                        <Text style={{ margin: 5 }}>{this.state.package.description}</Text>
                    </View>
                    <TouchableOpacity style={{ flexDirection: "row", marginTop: 20 }}
                        onPress={() => this.props.navigation.navigate("KullanimKosullari", this.state.rule)}>
                        <CheckBox checked={this.state.isAccepted}
                            onPress={() => this.setState({ isAccepted: !this.state.isAccepted })} />
                        <Text style={{ marginLeft: 20 }}>Satınalma kurallarını kabul ediyorum.</Text>
                    </TouchableOpacity>
                </View>
                <Button full info style={{ width: "100%" }}
                    onPress={this._satinAl}>
                    <Text>SATIN AL</Text>
                </Button>
            </View>
        );
    }
}
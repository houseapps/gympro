import React, { Component } from 'react';
import { View, ImageBackground, TouchableOpacity, PixelRatio, ScrollView, TouchableHighlight } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Label, Badge } from 'native-base';
import { Divider } from 'react-native-elements';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';

import WebClient from '../helper/webclient'
import MainHeaderBar from './MainHeaderBar';
import withPreventDoubleClick from './withPreventDoubleClick';

//const TouchableOpacityEx = withPreventDoubleClick(TouchableOpacity);

function mapStateToParams(state) {
  return { user: state.user.member };
}
let my = this;
@connect(mapStateToParams)
export default class Main extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: <MainHeaderBar />
  });

  constructor(props) {
    super(props);
    my = this;
    this.webClient = new WebClient();
    this.state = {
      unreadMessageCount: 0
    }
    this.state = {showOverlay: false}
    this._navigate = this._navigate.bind(this);
  }

  componentWillMount() {
    let stat = this.props.navigation.state.params;
    this.setState({ unreadMessageCount: 0 });
    if (typeof(stat) === 'undefined') {
      this.webClient.get(`Mobile/Messages/Inbox/1`)
        .then(response => {
          this.setState({ unreadMessageCount: response.data.data.length });
        });
    }
  }

  _navigate(path) {
    /*
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: path })
      ]
    });
    */
    this.props.navigation.navigate(path);
  }


  render() {
    return (
      <Grid style={{ marginBottom: 10 }}>
        <Row style={{ flex: 1 }}>
          <View style={{ position: "absolute", bottom: 10, left: 10 }}>
            <Label style={{ color: "black", fontSize: 24 }}>Hoşgeldin</Label>
            <Label style={{ color: "black", fontSize: 24, fontWeight: "bold" }}>{this.props.user.firstName}.</Label>
          </View>
          {
            this.state.unreadMessageCount != 0 ?
              <TouchableOpacity style={{
                position: "absolute", bottom: 10, left: "80%",
                width: "100%", height: "100%"
              }}
                onPress={() => this._navigate("Mesajlarim")}>
                <ImageBackground source={require("../assets/yeniMesaj.png")}
                  style={{ width: 50, height: 50, position: "absolute", bottom: 5, left: 5 }}>
                  <Badge style={{ position: "absolute", top: -8, left: "60%" }}>
                    <Text style={{ color: "white", fontSize:14 }}>{this.state.unreadMessageCount}</Text>
                  </Badge>
                </ImageBackground>
              </TouchableOpacity>
              : null
          }
        </Row>
        <Row style={{ flex: 4, maxHeight: 340 }}>
          <Grid>
            <Row>
              <Grid>
                <Col size={2.07} style={{ backgroundColor: "rgb(245,195,91)", margin: 5 }}>
                  <TouchableOpacity style={{ width: "100%", height: "100%" }}
                    onPress={() => this._navigate("HemenRezarvasyonlarim")}>
                    <View style={{ margin: 5 }}>
                      <Label style={{ color: "black", fontSize:14, }}>Hemen Rezervasyon</Label>
                    </View>
                    <ImageBackground source={require("../assets/hemenRezervasyon.png")}
                      style={{ width: 50, height: 50, position: "absolute", bottom: 5, left: 5 }} />
                  </TouchableOpacity>
                </Col>
                <Col size={1} style={{ backgroundColor: "rgb(239,142,77)", margin: 5 }}>
                  <TouchableOpacity style={{ width: "100%", height: "100%" }}
                    onPress={() => this._navigate("SporProgramim")}>
                    <View style={{ margin: 5 }}>
                      <Label style={{ color: "black", fontSize:14 }}>Spor Programım</Label>
                    </View>
                    <ImageBackground source={require("../assets/sporProgramin.png")}
                      style={{ width: 42, height: 46, position: "absolute", bottom: 5, left: 5 }} />
                  </TouchableOpacity>
                </Col>
              </Grid>
            </Row>
            <Row>
              <Grid>
                <Col size={1} style={{ backgroundColor: "rgb(239,142,77)", margin: 5 }}>
                  <TouchableOpacity style={{ width: "100%", height: "100%" }}
                    onPress={() => this._navigate("Olculerim")}>
                    <View style={{ margin: 5 }}>
                      <Label style={{ color: "black", fontSize:14 }}>Ölçümlerim</Label>
                    </View>
                    <ImageBackground source={require("../assets/olculerim.png")}
                      style={{ width: 40, height: 40, position: "absolute", bottom: 5, left: 5 }} />
                  </TouchableOpacity>
                </Col>
                <Col size={1} style={{ backgroundColor: "rgb(239,142,77)", margin: 5 }}>
                  <TouchableOpacity style={{ width: "100%", height: "100%" }}
                    onPress={() => this._navigate("SatinAl")}>
                    <View style={{ margin: 5 }}>
                      <Label style={{ color: "black", fontSize:14 }}>Satın al</Label>
                    </View>
                    <ImageBackground source={require("../assets/satinAl.png")}
                      style={{ width: 50, height: 40, position: "absolute", bottom: 5, left: 5 }} />
                  </TouchableOpacity>
                </Col>
                <Col size={1} style={{ backgroundColor: "rgb(239,142,77)", margin: 5 }}>
                  <TouchableOpacity style={{ width: "100%", height: "100%" }}
                    onPress={() => this._navigate("DersProgramim")}>
                    <View style={{ margin: 5 }}>
                      <Label style={{ color: "black", fontSize:14 }}>Ders Programı</Label>
                    </View>
                    <ImageBackground source={require("../assets/dersProgrami.png")}
                      resizeMode="cover"
                      style={{ width: 32, height: 46, position: "absolute", bottom: 5, left: 5 }} />
                  </TouchableOpacity>
                </Col>
              </Grid>
            </Row>
            <Row>
              <Grid>
                <Col size={1} style={{ backgroundColor: "rgb(239,142,77)", margin: 5 }}>
                  <TouchableOpacity style={{ width: "100%", height: "100%" }}
                    onPress={() => this._navigate("Mesajlarim")}>
                    <View style={{ margin: 5 }}>
                      <Label style={{ color: "black", fontSize: 14 }}>Mesaj Gönder</Label>
                    </View>
                    <ImageBackground source={require("../assets/mesaj.png")}
                      style={{ width: 50, height: 40, position: "absolute", bottom: 5, left: 5 }} />
                  </TouchableOpacity>
                </Col>
                <Col size={2.07} style={{ backgroundColor: "rgb(182,201,71)", margin: 5 }}>
                  <TouchableOpacity style={{ width: "100%", height: "100%" }}
                    onPress={() => this._navigate("KareKodIleGecis")}>
                    <View style={{ margin: 5 }}>
                      <Label style={{ color: "black", fontSize: 14 }}>Karekod oluştur</Label>
                    </View>
                    <ImageBackground source={require("../assets/karekod.png")}
                      style={{ width: 50, height: 50, position: "absolute", bottom: 5, left: 5 }} />
                  </TouchableOpacity>
                </Col>
              </Grid>
            </Row>
          </Grid>
        </Row>
      </Grid>      
    );
  }
}
import React, { Component } from 'react';
import { View, ScrollView, ImageBackground } from 'react-native';
import { Tab, Tabs, TabHeading, Text, Button } from 'native-base';

import HeaderBar from '../HeaderBar';
import WebClient from '../../helper/webclient';

const title = "Hakkında";

export default class Index extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor() {
        super();
    }

    render() {
        return (
            <View style={{ position: "relative", width: "100%", height: "100%", justifyContent: "space-around", alignItems: "center" }}>
                <View style={{ marginTop: 50, justifyContent: "space-around", alignItems: "center" }}>
                    <ImageBackground
                        style={{ width: 200, height: 50 }}
                        source={require("../../assets/gymProLogo.png")} />
                    <Text style={{ fontSize: 16, textAlign: "left" }} note>Version 1.0.0.1</Text>
                </View>
                <View style={{ marginTop: 20 }}>
                    <View style={{ marginBottom: 30, justifyContent: "space-around", alignItems: "center" }}>
                        <Text style={{ textAlign: "center" }}>GymPro bir</Text>
                        <Text style={{ textAlign: "center" }}>Argedan Bilişim Limited Şirketi</Text>
                        <Text style={{ textAlign: "center" }}>markasıdır</Text>
                    </View>
                    <View style={{ marginBottom: 30, justifyContent: "space-around", alignItems: "center", marginBottom: 50 }}>
                        <Text style={{ textAlign: "center" }}>Tüm hakları saklıdır.</Text>
                        <Text style={{ textAlign: "center" }}>Uygulamada bulunan görseller,</Text>
                        <Text style={{ textAlign: "center" }}>yazılar Argedan'ın yazılı izni</Text>
                        <Text style={{ textAlign: "center" }}>olmadan kullanılamaz.</Text>
                    </View>
                </View>
                <View style={{ flex: 1 }} />
                <View style={{ position: "absolute", left: 0, bottom: 10 }}>
                    <ImageBackground
                        source={require("../../assets/hakkindaLogo.png")}
                        style={{ width: 190, height: 40 }} />
                </View>
            </View>
        );
    }
}
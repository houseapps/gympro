import React, { Component } from 'react';
import { View, ScrollView, ImageBackground, TouchableOpacity, Alert, KeyboardAvoidingView } from 'react-native';
import { Form, Item, Input, Label, Button, Text, CheckBox } from 'native-base';
import { NavigationActions } from 'react-navigation';

import WebClient from '../../../helper/webclient';

export default class ForgetPassword extends Component {

    constructor() {
        super();
        this.state = {
            smsOnayKodu: "",
            yeniSifre: "",
            yeniSifreTekrar: "",
        }

        this.webClient = new WebClient({
            "baseURL": "https://api.fitnessonline.net",
            "crossDomain": true,
            "headers": {
                "content-type": "application/json"
            }
        });
        this._changePassword = this._changePassword.bind(this);
        this._goLogin = this._goLogin.bind(this);
    }

    _changePassword() {
        if (this.state.yeniSifre == this.state.yeniSifreTekrar) {
            this.webClient.post(`v1/Mobile/Password/Complated`, {
                CompanyCode: this.props.navigation.state.params.isletmeKodu,
                UserName: this.props.navigation.state.params.kullaniciAdi,
                Password: this.state.yeniSifre,
                ActivationCode: this.state.smsOnayKodu
            }, true, false)
                .then(response => {
                    Alert.alert(
                        'Bilgi',
                        'Şifreniz başarıyla değiştirilmiştir.',
                        [
                            { text: 'Tamam', onPress: () => this._goLogin() }
                        ],
                        { cancelable: false }
                    )
                });
        }
        else {
            Alert.alert(
                'Uyarı',
                'Girmiş olduğunuz şifreler uyuşmamaktadır.',
                [
                    { text: 'Tamam' }
                ],
                { cancelable: false }
            );
        }
    }

    _goLogin() {
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: "Login",
                    params: { kullaniciAdi: this.props.navigation.state.params.kullaniciAdi, yeniSifre: this.state.yeniSifre }
                })
            ]
        });
        this.props.navigation.dispatch(resetAction);
    }

    render() {
        return (
            <Form style={{ alignItems: "center" }}>
                <Text style={{ margin: 10, marginTop: 30, fontSize: 36, textAlign: "center" }}>Şifre Yenileme</Text>
                <Text style={{ margin: 10, fontSize: 18, textAlign: "center" }}>{this.props.navigation.state.params.text}</Text>
                <Item floatingLabel last>
                    <Label>Sms Onay Kodu</Label>
                    <Input value={this.state.smsOnayKodu}
                        onChangeText={(text) => this.setState({ smsOnayKodu: text })} />
                </Item>
                <Item floatingLabel last>
                    <Label>Yeni Şifre</Label>
                    <Input value={this.state.yeniSifre} secureTextEntry
                        onChangeText={(text) => this.setState({ yeniSifre: text })} />
                </Item>
                <Item floatingLabel last>
                    <Label>Yeni Şifre (Tekrar)</Label>
                    <Input value={this.state.yeniSifreTekrar} secureTextEntry
                        onChangeText={(text) => this.setState({ yeniSifreTekrar: text })} />
                </Item>
                <View style={{ margin: 10, flexDirection: "row", position: "relative", bottom: 0 }}>
                    <Button full info style={{ width: "100%" }}
                        onPress={this._changePassword}>
                        <Text>GÖNDER</Text>
                    </Button>
                </View>
            </Form>
        );
    }
}
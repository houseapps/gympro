import React, { Component } from 'react';
import { View, ScrollView, ImageBackground, TouchableOpacity } from 'react-native';
import { Form, Item, Input, Label, Button, Text, CheckBox } from 'native-base';
import { NavigationActions } from 'react-navigation';

import WebClient from '../../../helper/webclient';

export default class Register1 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            aktivasyonKodu: "",
            isPrivateApp: false
        }

        this.webClient = new WebClient({
            "baseURL": "https://api.fitnessonline.net",
            "crossDomain": true,
            "headers": {
                "content-type": "application/json"
            }
        });
        this._nextScreen = this._nextScreen.bind(this);
        this._goLogin = this._goLogin.bind(this);
    }

    _nextScreen() {
        let company_code = '';
        if (this.state.isPrivateApp) { company_code = '?companyCode=' + this.props.navigation.state.params.isletmeKodu; }
        console.log(company_code);
        this.webClient.get(`v1/Mobile/Activations/CheckCode/${this.state.aktivasyonKodu}${company_code}`, true, false)
            .then(response => {
                this.props.navigation.navigate("Register2", {
                    kullaniciAdi: this.props.navigation.state.params.kullaniciAdi,
                    isletmeKodu: this.props.navigation.state.params.isletmeKodu,
                    aktivasyonKodu: this.state.aktivasyonKodu,
                    ...response.data.data
                });
            });
    }

    _goLogin(){
        const resetAction = NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: "Login" })
          ]
        });
        this.props.navigation.dispatch(resetAction);
    }

    render() {
        return (
            <Form style={{ alignItems: "center" }}>
                <Text style={{ margin: 10, marginTop: 30, fontSize: 36, textAlign: "center" }}>Aktivasyon</Text>
                <View style={{ margin: 10, flexDirection: "row", justifyContent: "center" }}>
                    <Text style={{ fontSize: 18, textAlign: "center" }}>Zaten üyemisin?</Text>
                    <TouchableOpacity style={{ flexDirection: "row" }}
                        onPress={this._goLogin}>
                        <Text style={{ fontSize: 18, textAlign: "center", color: "rgb(60, 148,214)" }}> Giriş yap</Text>
                    </TouchableOpacity>
                </View>
                <Item floatingLabel last>
                    <Label>Aktivasyon Kodunuzu Giriniz</Label>
                    <Input value={this.state.aktivasyonKodu}
                        onChangeText={(text) => this.setState({ aktivasyonKodu: text })} />
                </Item>
                <View style={{ margin: 10, flexDirection: "row", position: "relative", bottom: 0 }}>
                    <Button full info style={{ width: "100%" }}
                        onPress={this._nextScreen}>
                        <Text>GÖNDER</Text>
                    </Button>
                </View>
            </Form>
        );
    }
}
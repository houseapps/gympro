import React, { Component } from 'react';
import { View, ScrollView, ImageBackground, TouchableOpacity, Alert, KeyboardAvoidingView } from 'react-native';
import { Form, Item, Input, Label, Button, Text, CheckBox } from 'native-base';
import { NavigationActions } from 'react-navigation';
import DeviceInfo from 'react-native-device-info';

import WebClient from '../../../helper/webclient';

export default class RegisterComplate extends Component {

    constructor() {
        super();
        this.state = {
            smsOnayKodu: ""
        }

        this.webClient = new WebClient({
            "baseURL": "https://api.fitnessonline.net",
            "crossDomain": true,
            "headers": {
                "content-type": "application/json"
            }
        });
        this._activate = this._activate.bind(this);
        this._goLogin = this._goLogin.bind(this);
    }

    _activate() {
        this.webClient.post(`v1/Mobile/Devices/RegisterComplated`, {
            CompanyCode: this.props.navigation.state.params.CompanyCode,
            ActivationCode: this.state.smsOnayKodu,
            UserName: this.props.navigation.state.params.UserName,
            Password: this.props.navigation.state.params.Password,
            DeviceID: DeviceInfo.getUniqueID(),
            DeviceBrand: DeviceInfo.getBrand(),
            DeviceModel: DeviceInfo.getModel(),
            OsName: DeviceInfo.getSystemName(),
            OsVersion: DeviceInfo.getSystemVersion()

        }, true, false)
            .then(response => {
                Alert.alert(
                    'Bilgi',
                    'Cihaz aktivasyonunuz başarıyla tamamlanmıştır.',
                    [
                        { text: 'Tamam', onPress: () => this._goLogin() }
                    ],
                    { cancelable: false }
                )
            });
    }

    _goLogin() {
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: "Login",
                    params: {
                        kullaniciAdi: this.props.navigation.state.params.UserName,
                        yeniSifre: this.props.navigation.state.params.Password
                    }
                })
            ]
        });
        this.props.navigation.dispatch(resetAction);
    }

    render() {
        return (
            <Form style={{ alignItems: "center" }}>
                <Text style={{ margin: 10, marginTop: 30, marginBottom: 0, fontSize: 36, textAlign: "center" }}>Cihaz Aktivasyonu</Text>
                <Text style={{ margin: 10, fontSize: 14, textAlign: "center" }}>{this.props.navigation.state.params.text}</Text>
                <Item floatingLabel last>
                    <Label>Sms Onay Kodu</Label>
                    <Input value={this.state.smsOnayKodu}
                        onChangeText={(text) => this.setState({ smsOnayKodu: text })} />
                </Item>
                <View style={{ margin: 10, flexDirection: "row", position: "relative", bottom: 0 }}>
                    <Button full info style={{ width: "100%" }}
                        onPress={this._activate}>
                        <Text>GÖNDER</Text>
                    </Button>
                </View>
            </Form>
        );
    }
}
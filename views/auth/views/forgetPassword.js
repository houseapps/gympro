import React, { Component } from 'react';
import { View, ScrollView, ImageBackground, TouchableOpacity } from 'react-native';
import { Form, Item, Input, Label, Button, Text, CheckBox } from 'native-base';

import WebClient from '../../../helper/webclient';

export default class ForgetPassword extends Component {

    constructor(props) {
        super(props);
        this.state = {
            kullaniciAdi: props.navigation.state.params.kullaniciAdi,
            isletmeKodu: "",
        }

        this.webClient = new WebClient({
            "baseURL": "https://api.fitnessonline.net",
            "crossDomain": true,
            "headers": {
                "content-type": "application/json"
            }
        });
        this._nextScreen = this._nextScreen.bind(this);
    }

    _nextScreen() {
        this.webClient.post("v1/Mobile/Password/SendCode", {
            CompanyCode: this.state.isletmeKodu,
            UserName: this.state.kullaniciAdi
        }, true, false).then((response) => {
            this.props.navigation.navigate("NewPassword", {
                kullaniciAdi: this.state.kullaniciAdi,
                isletmeKodu: this.state.isletmeKodu,
                text: response.data.data
            });
        });
    }

    render() {
        return (
            <Form style={{ alignItems: "center" }}>
                <Text style={{ margin: 10, marginTop: 30, fontSize: 36, textAlign: "center" }}>Şifremi Unuttum</Text>
                <Item floatingLabel last>
                    <Label>Kullanıcı Adı (E-Posta Adresiniz)</Label>
                    <Input value={this.state.kullaniciAdi}
                        onChangeText={(text) => this.setState({ kullaniciAdi: text })} />
                </Item>
                <Item floatingLabel last>
                    <Label>İşletme Kodu</Label>
                    <Input value={this.state.isletmeKodu}
                        onChangeText={(text) => this.setState({ isletmeKodu: text })} />
                </Item>
                <View style={{ margin: 10, flexDirection: "row", position: "relative", bottom: 0 }}>
                    <Button full info style={{ width: "100%" }}
                        onPress={this._nextScreen}>
                        <Text>GÖNDER</Text>
                    </Button>
                </View>
            </Form>
        );
    }
}
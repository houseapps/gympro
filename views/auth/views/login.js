import React, { Component } from 'react';
import { View, ScrollView, ImageBackground, TouchableOpacity, Alert, AsyncStorage, KeyboardAvoidingView, ActivityIndicator } from 'react-native';
import { Form, Item, Input, Label, Button, Text, CheckBox } from 'native-base';
import DeviceInfo from 'react-native-device-info';

import WebClient from '../../../helper/webclient';
import * as AppActions from '../../../redux/actions/app';
let my = this;
export default class Login extends Component {
    constructor(props) {
        super(props);
        my = this;
        this.state = {
            kullaniciAdi: "",
            isletmeKodu: "",
            sifre: "",
            beniHatirla: false
        }

        this.state.showIndicator = true;

        this.webClient = new WebClient({
            "baseURL": "https://api.fitnessonline.net",
            "crossDomain": true,
            "headers": {
                "content-type": "application/json"
            }
        });
        this._girisYap = this._girisYap.bind(this);
        this._sendActivation = this._sendActivation.bind(this);
        this._registerDevice = this._registerDevice.bind(this);
    }

    componentDidMount() {
        setTimeout(() => {
            AsyncStorage.getItem("login_status").then(function(value){
                if (value) {
                    my._girisYap();
                }
                this.setTimeout(() => {
                  my.setState({showIndicator: false})
                }, 1000);

            });
        }, 500);

        if (this.props.navigation.state.params == undefined) {
            AsyncStorage.getItem("loginState").then((value) => {
                let oldState = JSON.parse(value);
                this.setState({
                    kullaniciAdi: oldState.kullaniciAdi,
                    isletmeKodu: oldState.isletmeKodu,
                    sifre: oldState.sifre,
                    beniHatirla: oldState.beniHatirla
                }, () => this.setState({
                    kullaniciAdi: this.state.kullaniciAdi,
                    isletmeKodu: this.state.isletmeKodu,
                    sifre: this.state.sifre,
                    beniHatirla: this.state.beniHatirla
                }));
            });
        }
        else {
            this.setState({
                kullaniciAdi: this.props.navigation.state.params.kullaniciAdi,
                sifre: this.props.navigation.state.params.yeniSifre,
                isletmeKodu: this.props.navigation.state.params.isletmeKodu,
                beniHatirla: this.state.beniHatirla
            }, () => this.setState({
                kullaniciAdi: this.state.kullaniciAdi,
                isletmeKodu: this.state.isletmeKodu,
                sifre: this.state.sifre,
                beniHatirla: this.state.beniHatirla
            }));
        }
    }

    _girisYap() {
        AsyncStorage.setItem("loginState", JSON.stringify(this.state));
        AppActions.login({
            CompanyCode: this.state.isletmeKodu,
            UserName: this.state.kullaniciAdi,
            Password: this.state.sifre,
            DeviceID: DeviceInfo.getUniqueID(),
            DeviceBrand: DeviceInfo.getBrand(),
            DeviceModel: DeviceInfo.getModel(),
            OsName: DeviceInfo.getSystemName(),
            OsVersion: DeviceInfo.getSystemVersion()
        }, this._sendActivation);
    }

    _sendActivation(data) {
        console.log(data);
        if (data.errorCode == 100) {
            this.props.navigation.navigate("Register2", {
                            kullaniciAdi: this.state.kullaniciAdi,
                            isletmeKodu: this.state.isletmeKodu,
                            aktivasyonKodu: this.state.sifre,
                            ...data.data
                        })
        } else {
            Alert.alert(
                'Bilgi',
                data.errorMessage,
                [
                    { text: 'Tamam', onPress: () => this._registerDevice() },
                    { text: 'İptal' }
                ],
                { cancelable: false }
            )
        }
    }   

    _registerDevice() {
        this.webClient.post("v1/Mobile/Devices/RegisterRequest", {
            CompanyCode: this.state.isletmeKodu,
            UserName: this.state.kullaniciAdi,
            Password: this.state.sifre,
            DeviceID: DeviceInfo.getUniqueID(),
            DeviceBrand: DeviceInfo.getBrand(),
            DeviceModel: DeviceInfo.getModel(),
            OsName: DeviceInfo.getSystemName(),
            OsVersion: DeviceInfo.getSystemVersion()
        }, true, false).then((response) => {
            this.props.navigation.navigate("RegisterComplate", {
                CompanyCode: this.state.isletmeKodu,
                UserName: this.state.kullaniciAdi,
                Password: this.state.sifre,
                DeviceID: DeviceInfo.getUniqueID(),
                DeviceBrand: DeviceInfo.getBrand(),
                DeviceModel: DeviceInfo.getModel(),
                OsName: DeviceInfo.getSystemName(),
                OsVersion: DeviceInfo.getSystemVersion(),
                text: response.data.data
            });
        });
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%", alignItems: "center", flex: 1, justifyContent: "center" }}>
            {
            this.state.showIndicator ?
            <View style={{ width: "100%", height: "100%", flex: 1, justifyContent: "center" }}>
                <ActivityIndicator size="large" color="#FFFFFF" />
            </View>
            :
            <Form style={{ width: "100%", height: "100%", justifyContent: "center", alignItems: "center" }} >
                <Text style={{ margin: 10, marginTop: 30, marginBottom: 0, fontSize: 36, textAlign: "center" }}>Giriş</Text>
                <Item floatingLabel last>
                    <Label>Kullanıcı Adı</Label>
                    <Input value={this.state.kullaniciAdi}
                        onChangeText={(text) => this.setState({ kullaniciAdi: text })} />
                </Item>
                <Item floatingLabel last>
                    <Label>İşletme Kodu</Label>
                    <Input value={this.state.isletmeKodu}
                        onChangeText={(text) => this.setState({ isletmeKodu: text })} />
                </Item>
                <Item floatingLabel last>
                    <Label>Şifreniz</Label>
                    <Input value={this.state.sifre} secureTextEntry
                        onChangeText={(text) => this.setState({ sifre: text })} />
                </Item>
                <View style={{ width: "100%", margin: 10, flexDirection: "row", justifyContent: "flex-start", paddingLeft: 10 }}>
                    <TouchableOpacity style={{ flexDirection: "row" }}
                        onPress={() => this.props.navigation.navigate("ForgetPassword", { kullaniciAdi: this.state.kullaniciAdi })}>
                        <Text style={{ fontSize: 16, textAlign: "center" }}>Eyvah! Şifremi unuttum!</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ margin: 10, flexDirection: "row", position: "relative", bottom: 0 }}>
                    <Button full info style={{ width: "100%" }}
                        onPress={this._girisYap}>
                        <Text>GİRİŞ</Text>
                    </Button>
                </View>
            </Form>
            }
            </View>
        );
    }
}

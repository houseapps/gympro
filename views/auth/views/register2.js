import React, { Component } from 'react';
import { View, ScrollView, ImageBackground, TouchableOpacity, KeyboardAvoidingView, Alert, Image } from 'react-native';
import { Form, Item, Input, Label, Button, Text, CheckBox } from 'native-base';
import { NavigationActions } from 'react-navigation';
import DeviceInfo from 'react-native-device-info';

import WebClient from '../../../helper/webclient';

export default class Register2 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            kullaniciAdi: "",
            yeniSifre: "",
            yeniSifreTekrar: "",
            isAccepted: false
        }

        this.webClient = new WebClient({
            "baseURL": "https://api.fitnessonline.net",
            "crossDomain": true,
            "headers": {
                "content-type": "application/json"
            }
        });
        this._nextScreen = this._nextScreen.bind(this);
        this._goLogin = this._goLogin.bind(this);
    }

    _nextScreen() {
        if (this.state.isAccepted) {
            if (this.state.yeniSifre == this.state.yeniSifreTekrar) {
                this.webClient.post(`v1/Mobile/Activations/Complate`, {
                    CompanyCode: this.props.navigation.state.params.isletmeKodu,
                    ActivationCode: this.props.navigation.state.params.aktivasyonKodu,
                    UserName: this.state.kullaniciAdi,
                    Password: this.state.yeniSifre,
                    DeviceID: DeviceInfo.getUniqueID(),
                    DeviceBrand: DeviceInfo.getBrand(),
                    DeviceModel: DeviceInfo.getModel(),
                    OsName: DeviceInfo.getSystemName(),
                    OsVersion: DeviceInfo.getSystemVersion()
                }, true, false)
                    .then(response => {
                        console.log(response.data.data);
                        Alert.alert(
                            'Bilgi',
                            response.data.data,
                            [
                                { text: 'Tamam', onPress: () => this._goLogin() }
                            ],
                            { cancelable: false }
                        )
                    });
            }
            else {
                Alert.alert(
                    'Uyarı',
                    'Girmiş olduğunuz şifreler uyuşmamaktadır.',
                    [
                        { text: 'Tamam' }
                    ],
                    { cancelable: false }
                );
            }
        } else {
            Alert.alert(
                'Uyarı',
                'Üyelik kurallarını kabul etmeniz gerekmektedir.',
                [
                    { text: 'Tamam' }
                ],
                { cancelable: false }
            );
        }
    }

    _goLogin() {
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: "Login",
                    params: { kullaniciAdi: this.state.kullaniciAdi, yeniSifre: this.state.yeniSifre, isletmeKodu: this.props.navigation.state.params.isletmeKodu }
                })
            ]
        });
        this.props.navigation.dispatch(resetAction);
    }

    render() {
        return (             
            <Form style={{ alignItems: "center" }}>       
                <Image
                  style={{ width: 80, height: 80 }}
                  source={require("../../../assets/logo.png")}
                />                
                <Text style={{marginTop: 10, fontSize: 18, textAlign: "center" }}>Teşekkürler {this.props.navigation.state.params.firstName} {this.props.navigation.state.params.lastName}</Text>
                <Text style={{marginTop: 5, fontSize: 11, textAlign: "center" }}>Kimliğinizi doğrulamak için e-posta adresinizi ve şifrenizi yazınız.</Text>
                <Item floatingLabel last>
                    <Label>E-Posta Adresiniz (Yeni kullanıcı adınızdır)</Label>
                    <Input value={this.state.kullaniciAdi}
                        onChangeText={(text) => this.setState({ kullaniciAdi: text })} />
                </Item>
                <Item floatingLabel last>
                    <Label>Şifreniz</Label>
                    <Input value={this.state.yeniSifre} secureTextEntry
                        onChangeText={(text) => this.setState({ yeniSifre: text })} />
                </Item>
                <Item floatingLabel last>
                    <Label>Şifreniz (Tekrar)</Label>
                    <Input value={this.state.yeniSifreTekrar} secureTextEntry
                        onChangeText={(text) => this.setState({ yeniSifreTekrar: text })} />
                </Item>
                <View style={{ width: "100%", flexDirection: "row", alignSelf: "flex-start" }}>
                <TouchableOpacity style={{ flexDirection: "row", marginTop: 10 }}
                    onPress={() => this.props.navigation.navigate("KullanimKosullari", {
                        doc: this.props.navigation.state.params.memberPolicy,
                        accept: () => { this.setState({ isAccepted: true }) },
                        reject: () => { this.setState({ isAccepted: false }) }
                    })}>
                    <CheckBox checked={this.state.isAccepted}
                        onPress={() => this.setState({ isAccepted: !this.state.isAccepted })} />
                    <Text style={{ marginLeft: 20 }}>Üyelik kurallarını kabul ediyorum.</Text>
                </TouchableOpacity>
                </View>
                <View style={{ margin: 10, flexDirection: "row", position: "relative", bottom: 0 }}>
                    <Button full info style={{ width: "100%" }}
                        onPress={this._nextScreen}>
                        <Text>GÖNDER</Text>
                    </Button>
                </View>
                <View style={{ margin: 10, flexDirection: "row", justifyContent: "center" }}>
                    <Text style={{ fontSize: 16, textAlign: "center" }}>Zaten üye misin?</Text>
                    <TouchableOpacity style={{ flexDirection: "row" }}
                        onPress={this._goLogin}>
                        <Text style={{ fontSize: 18, textAlign: "center", color: "rgb(60, 148,214)" }}> Giriş yap</Text>
                    </TouchableOpacity>
                </View>                
            </Form>
        );
    }
}
import React, { Component } from 'react';
import { StackNavigator, NavigationActions } from "react-navigation";

import Login from './views/login';
import ForgetPassword from './views/forgetPassword';
import NewPassword from './views/newPassword';
import Register1 from './views/register1';
import Register2 from './views/register2';
import RegisterComplate from './views/registerComplate';
import KullanimKosullari from './views/kullanimKosullari';

import * as AppActions from '../../redux/actions/app';

const navigateOnce = (getStateForAction) => (action, state) => {
    AppActions.isLoading();
    const { type, routeName } = action;
    let result = (
        state &&
        type === NavigationActions.NAVIGATE &&
        routeName === state.routes[state.routes.length - 1].routeName
    ) ? null : getStateForAction(action, state);
    AppActions.isLoaded();
    return result;
};

const AuthNavigator = StackNavigator(
    {
        Login: { screen: Login },
        ForgetPassword: { screen: ForgetPassword },
        NewPassword: { screen: NewPassword },
        Register1: { screen: Register1 },
        Register2: { screen: Register2 },
        RegisterComplate: { screen: RegisterComplate },
        KullanimKosullari: { screen: KullanimKosullari }
    }, {
        headerMode: 'none',
        mode: "card",
        cardStyle: {
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "white"
        }
    }
);
AuthNavigator.router.getStateForAction = navigateOnce(AuthNavigator.router.getStateForAction);

export default AuthNavigator;
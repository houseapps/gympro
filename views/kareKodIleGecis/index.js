import React, { Component } from 'react';
import { View, Platform } from 'react-native';
import { Text } from 'native-base'
import QRCode from 'react-native-qrcode-svg';
import ProgressCircle from 'react-native-progress-circle';
import PercentageCircle from 'react-native-percentage-circle';

import HeaderBar from '../HeaderBar';
import webClient from '../../helper/webclient';

const timer = require('react-native-timer');
const title = "Kare kod oluştur";

export default class Index extends Component {
    static _self;

    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation}
            title={title}
            cb={() => { _self.componentWillUnmount() }} />
    });

    constructor(props) {
        super(props);
        this.webClient = new webClient();
        this.state = {
            counter: 5,
            percent: 100,
            qr: undefined
        }

        this._startTimer = this._startTimer.bind(this);
        this._getQrCode = this._getQrCode.bind(this);
        _self = this;
    }

    componentWillMount() {
        this._getQrCode();
    }

    _getQrCode() {
        this.webClient.get(`Mobile/QrCodeGenerate`)
            .then(response => {
                this.setState({
                    counter: 5,
                    percent: 100,
                    qr: response.data.data
                }, this._startTimer);
            });
    }

    _startTimer() {
        timer.setInterval(
            this, '', () => {
                if (this.state.counter == 0) {
                    timer.clearTimeout(this);
                    this._getQrCode();
                }
                else {
                    this.setState({
                        counter: (this.state.counter - 1),
                        percent: (this.state.counter - 1) * 20
                    });
                }
            }, 1000);
    }

    componentWillUnmount() {
        timer.clearInterval(this);
        timer.clearTimeout(this);
        console.log(this.state);
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%", justifyContent: "space-between", alignItems: "center" }}>
                <View style={{ margin: 10 }}>
                    <Text style={{ fontSize: 22, fontWeight: "400", color: "rgb(65,159,221)" }}>{"Kodun geçerlilik süresi"}</Text>
                </View>
                <View style={{ margin: 10 }}>
                {
                	Platform.OS === 'ios' ?
                    <ProgressCircle
                        borderWidth={10}
                        radius={60}
                        percent={this.state.percent}
                        shadowColor="white"
                        color="rgb(110,180,216)"
                        bgColor="white">
                        <Text style={{ fontSize: 62, fontWeight: "400" }}>{this.state.counter}</Text>
                    </ProgressCircle>    
                    :
                    <PercentageCircle 
                        borderWidth={10}
                        radius={60} 
                        bgColor="#ffffff"
                        percent={this.state.percent} 
                        color={"rgb(110,180,216)"}>
                        
                        <Text style={{ fontSize: 62, fontWeight: "400" }}>{this.state.counter}</Text>

                    </PercentageCircle> 
                }
                </View>
                <View style={{
                    flex: 1, width: "100%", height: "100%", margin: 10,
                    justifyContent: "space-around", alignItems: "center"
                }}>
                    <QRCode
                        value={this.state.qr}
                        size={180} />
                </View>
            </View>
        );
    }
}
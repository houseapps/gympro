import React, { Component } from 'react';
import { View, ImageBackground } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { Container, Header, Content, Tab, Tabs, TabHeading, Text, Button } from 'native-base';

import HeaderBar from '../../HeaderBar'

const title = "Bilgilendirme";

export default class Bilgilendirme extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar isShowBack={false} navigation={navigation} title={title} />
    });

    constructor(props) {
        super(props);
        this._navigate = this._navigate.bind(this);
    }

    _navigate(path) {
        let resetAction;
        if (path == "Main") {
            resetAction = NavigationActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({ routeName: "Main" })
                ]
            });
        }
        else {
            resetAction = NavigationActions.reset({
                index: 1,
                actions: [
                    NavigationActions.navigate({ routeName: "Main" }),
                    NavigationActions.navigate({ routeName: path })
                ]
            });
        }
        this.props.navigation.dispatch(resetAction);
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%", flexDirection: "column", justifyContent: "space-between", alignItems: "center" }}>
                <ImageBackground source={require("../../../assets/ok.png")}
                    style={{ marginTop: 50, width: 180, height: 180 }} />
                <Text style={{ fontSize: 28, fontWeight: "bold" }}>İşlem Başarılı</Text>
                <View>
                    <Text>Rezervasyon işleminiz başarı ile tamamlandı.</Text>
                    <Text>Rezervasyonlarım sayfasından takibini yapabilirsiniz.</Text>
                </View>
                <Button full info style={{ width: "100%" }}
                    onPress={() => this._navigate("Rezarvasyonlarim")}>
                    <Text>REZERVASYONLARIMA GİT</Text>
                </Button>
                <Button full info style={{ width: "100%" }}
                    onPress={() => this._navigate("Main")}>
                    <Text>ANASAYFA</Text>
                </Button>
            </View>
        );
    }
}
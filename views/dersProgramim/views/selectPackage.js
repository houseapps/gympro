import React, { Component } from 'react';
import { View, TouchableOpacity, ScrollView } from 'react-native';
import {
    Container, Header, Content, Tab, Tabs, TabHeading, Text,
    CheckBox, Button, List, ListItem,
    Left, Right, Body
} from 'native-base';
import { Divider } from 'react-native-elements';

import WebClient from '../../../helper/webclient';
import * as Actions from '../../../redux/actions/app';

import HeaderBar from '../../HeaderBar';
import Summary from '../components/summary';

const title = "Rezervasyon Yap";

export default class SelectPackage extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor(props) {
        super(props);
        this.webClient = new WebClient();
        let program = props.navigation.state.params.program;
        let place = props.navigation.state.params.place;
        this.state = {
            program: program,
            place: place,
            packages: [],
            selectedPackage: undefined,
            rule: {
                accept: () => { this.setState({ isAccepted: true }) },
                reject: () => { this.setState({ isAccepted: false }) },
                doc: ""
            },
            isAccepted: false
        }

        this._setPackage = this._setPackage.bind(this);
        this._goNextScreen = this._goNextScreen.bind(this);
        this._getPackageColor = this._getPackageColor.bind(this);
    }

    componentWillMount() {
        this.webClient.get(`Mobile/StudioReservations/Packages/${this.state.program.serviceID}`)
            .then((response) => {
                response.data.data = response.data.data.map(x => { return { ...x, endDate: new Date(x.endDate) } });
                this.setState({
                    packages: response.data.data
                });
            });

        this.webClient.get(`Mobile/StudioReservations/Rules`)
            .then((response) => {
                this.setState({
                    rule: {
                        accept: () => { this.setState({ isAccepted: true }) },
                        reject: () => { this.setState({ isAccepted: false }) },
                        doc: response.data.data
                    }
                });
            });
    }

    _getPackageColor(item) {
        if (item === this.state.selectedPackage) {
            return "rgb(238,143,77)";
        }
        else {
            return "white";
        }
    }

    _setPackage(item) {
        if (this.state.selectedPackage === item) {
            this.setState({ selectedPackage: undefined });
        }
        else {
            this.setState({ selectedPackage: item });
        }
    }

    _goNextScreen() {
        if (this.state.selectedPackage && this.state.isAccepted) {
            this.webClient.post(`Mobile/StudioReservations`, {
                ProgramID: this.state.program.programID,
                PackageID: this.state.selectedPackage.packageID,
                PlaceID: this.state.place === undefined ? 0 : this.state.place.placeID,
                IpAddress: "127.0.0.1"
            })
                .then(response => {
                    this.props.navigation.navigate("Bilgilendirme")
                });
        }
        else if (!this.state.isAccepted) {
            Actions.alert("Hata", "Kullanım koşullarını kabul etmelisiniz.");
        }
        else {
            Actions.alert("Hata", "Paket seçmelisiniz.");
        }
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%", }}>
                <Summary program={this.state.program} place={this.state.place} />
                <TouchableOpacity style={{ marginTop: 5, flexDirection: "row" }}
                    onPress={() => this.props.navigation.navigate("KullanimKosullari", this.state.rule)}>
                    <CheckBox checked={this.state.isAccepted} onPress={() => this.setState({ isAccepted: !this.state.isAccepted })} />
                    <Text style={{ marginLeft: 15, fontSize: 15 }}>Rezervasyon kurallarını kabul ediyorum.</Text>
                </TouchableOpacity>
                <Divider style={{ backgroundColor: 'black', width: "100%", height: 1, marginTop: 5 }} />
                <View style={{ flex: 1 }} >
                    <Text style={{ margin: 5, fontSize: 14, color: "rgb(65,159,221)", fontWeight: "bold" }}>
                        Lütfen bir paket seçiniz
                    </Text>
                    <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
                        <List>
                            {
                                this.state.packages.map((item, index) =>
                                    <ListItem key={index} style={{ marginLeft: -5, backgroundColor: this._getPackageColor(item) }}
                                        onPress={() => this._setPackage(item)}>
                                        <View style={{ marginLeft: 13 }}>
                                            <CheckBox checked={item == this.state.selectedPackage} onPress={() => this._setPackage(item)} />
                                        </View>
                                        <Body>
                                            <Text style={{ margin: 2, fontWeight: "bold" }}>{item.packageName}</Text>
                                            <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>
                                                <Text style={{ margin: 2, fontSize: 14 }}>Kalan: {item.remainingCredit}</Text>
                                                <Text style={{ margin: 2, fontSize: 14 }}>
                                                    Son kullanım: {("00" + item.endDate.getDate()).slice(-2) + "-" + ("00" + (item.endDate.getMonth() + 1)).slice(-2) + "-" + item.endDate.getFullYear()}
                                                </Text>
                                            </View>
                                        </Body>
                                    </ListItem>
                                )
                            }
                        </List>
                    </ScrollView>
                </View>
                <Button full info style={{ width: "100%" }}
                    onPress={this._goNextScreen}>
                    <Text>REZERVASYON</Text>
                </Button>
            </View>
        );
    }
}
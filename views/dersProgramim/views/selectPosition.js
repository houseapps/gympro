import React, { Component } from 'react';
import { View, ScrollView, WebView, TouchableOpacity, PixelRatio, Dimensions } from 'react-native';
import { Container, Header, Content, Tab, Tabs, TabHeading, Text, Button, Badge } from 'native-base';
import { Divider } from 'react-native-elements';
import PinchZoomView from 'react-native-pinch-zoom-view';

import WebClient from '../../../helper/webclient';
import * as Actions from '../../../redux/actions/app';

import HeaderBar from '../../HeaderBar';
import Summary from '../components/summary';

const { width, height } = Dimensions.get('window');

//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const scale = size => width / guidelineBaseWidth * size;
const verticalScale = size => height / guidelineBaseHeight * size;
const moderateScale = (size, factor = 0.5) => size + (scale(size) - size) * factor;

const title = "Rezervasyon Yap";

export default class SelectPosition extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor(props) {
        super(props);
        this.webClient = new WebClient();
        let program = props.navigation.state.params;
        this.state = {
            program: program,
            places: [],
            selectedPlace: undefined
        }

        this._getPlaceColor = this._getPlaceColor.bind(this);
        this._goNextScreen = this._goNextScreen.bind(this);
        this._setPlace = this._setPlace.bind(this);
    }

    componentWillMount() {
        this.webClient.get(`Mobile/StudioReservations/Places/${this.state.program.programID}`)
            .then((response) => {
                this.setState({
                    places: response.data.data
                });
            });
    }

    _getPlaceColor(place) {
        if (place === this.state.selectedPlace) {
            return "rgb(238,143,77)";
        }
        else if (place.placeType == 1) {
            if (place.placeStatus == 0) {
                return "rgb(186,215,0)";
            }
            else {
                return "rgb(204,204,204)";
            }
        }
        else {
            return "rgb(79,186,209)";
        }
    }

    _setPlace(place) {
        if (place.placeType == 1 && place.placeStatus == 0) {
            if (place === this.state.selectedPlace) {
                this.setState({ selectedPlace: undefined });
            }
            else {
                this.setState({ selectedPlace: place });
            }
        }
    }

    _goNextScreen() {
        if (this.state.selectedPlace) {
            this.props.navigation.navigate("SelectPackage", { program: this.state.program, place: this.state.selectedPlace })
        }
        else {
            Actions.alert("Hata", "Yer seçmelisiniz.");
        }
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%" }}>
                <View style={{ width: "100%", backgroundColor: "white" }}>
                    <Summary program={this.state.program} />
                    <Divider style={{ backgroundColor: 'black', width: "100%", height: 1, marginTop: 5 }} />
                </View>
                <View style={{ width: "100%", backgroundColor: "white" }}>
                    <Text style={{ margin: 5, fontSize: 14, color: "rgb(65,159,221)", fontWeight: "bold" }}>
                        Lütfen yer seçiniz
                </Text>
                </View>
                <PinchZoomView style={{ flex: 1, width: "200%", height: "200%", zIndex: -99 }} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    {
                        this.state.places.map((place, index) =>
                            <TouchableOpacity
                                onPress={() => this._setPlace(place)}
                                style={{
                                    width: scale(30), height: scale(30),
                                    backgroundColor: this._getPlaceColor(place), borderRadius: scale(15),
                                    flexDirection: "row", justifyContent: "space-around", alignItems: "center",
                                    position: "absolute", left: scale(place.mLeft / 1.70), top: scale(place.mTop / 2.00)
                                }}>
                                <Text style={{ color: "white" }}>{place.placeNO}</Text>
                            </TouchableOpacity>
                        )
                    }
                </PinchZoomView>
                <View style={{ width: "100%", backgroundColor: "white" }}>
                    <View style={{ margin: 5, flexDirection: "row", justifyContent: "space-around", alignItems: "center" }}>
                        <View style={{ flexDirection: "row", justifyContent: "space-around", alignItems: "center" }}>
                            <View
                                style={{ width: 20, height: 20, backgroundColor: "rgb(186,215,0)", borderRadius: 10 }} />
                            <Text style={{ color: "black", fontSize: 12 }}> Seçilebilir</Text>
                        </View>
                        <View style={{ flexDirection: "row", justifyContent: "space-around", alignItems: "center" }}>
                            <View
                                style={{ width: 20, height: 20, backgroundColor: "rgb(204,204,204)", borderRadius: 10 }} />
                            <Text style={{ color: "black", fontSize: 12 }}> Dolu</Text>
                        </View>
                        <View style={{ flexDirection: "row", justifyContent: "space-around", alignItems: "center" }}>
                            <View
                                style={{ width: 20, height: 20, backgroundColor: "rgb(238,143,77)", borderRadius: 10 }} />
                            <Text style={{ color: "black", fontSize: 12 }}> Seçili</Text>
                        </View>
                        <View style={{ flexDirection: "row", justifyContent: "space-around", alignItems: "center" }}>
                            <View
                                style={{ width: 20, height: 20, backgroundColor: "rgb(79,186,209)", borderRadius: 10 }} />
                            <Text style={{ color: "black", fontSize: 12 }}> Eğitmen</Text>
                        </View>
                    </View>
                </View>
                <Button full info style={{ width: "100%" }}
                    onPress={this._goNextScreen}>
                    <Text>DEVAM ET</Text>
                </Button>
            </View>
        );
    }
}
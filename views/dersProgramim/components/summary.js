import React, { Component } from 'react';
import { View } from 'react-native';
import { Container, Header, Content, Tab, Tabs, TabHeading, Text } from 'native-base';

export default class Summary extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View {...this.props} style={{ alignItems: "center" }}>
                <Text style={{ fontSize: 16, color: "rgb(228,130,48)" }}>{this.props.program.programDateFormatted}</Text>
                <Text style={{ fontSize: 16, color: "rgb(228,130,48)" }}>
                    {this.props.program.programStartFormatted} - {this.props.program.programFinishFormatted}
                </Text>
                <Text style={{ fontSize: 16, color: "rgb(65,159,221)" }}>{this.props.program.trainerName}</Text>
                <Text style={{ fontSize: 16, color: "rgb(65,159,221)" }}>{this.props.program.serviceName}</Text>
                <View style={{ flexDirection: "row" }}>
                    <Text style={{ fontSize: 16, color: "rgb(228,130,48)" }}>Stüdyo : {this.props.program.studioName}</Text>
                    {
                        this.props.place != undefined ?
                            <Text style={{ fontSize: 16, color: "rgb(228,130,48)" }}>, Yer : {this.props.place.placeNO}</Text> :
                            null
                    }
                </View>
            </View>
        );
    }
}
import React, { Component } from 'react';
import { View, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import { Text, Icon } from 'native-base';
import PopupDialog from 'react-native-popup-dialog';
import { Calendar, } from 'react-native-calendars';

import WebClient from '../../helper/webclient'
import HeaderBar from '../HeaderBar'

const title = "Ders Programım";

export default class Index extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor(props) {
        super(props);
        this.webClient = new WebClient();

        this.state = {
            currentDate: new Date(),
            selectedDate: new Date(),
            selectedDateProgram: []
        }

        this.openProgramDetailPage = this.openProgramDetailPage.bind(this);
        this.getDersProgrami = this.getDersProgrami.bind(this);
        this.getMonthText = this.getMonthText.bind(this);
        this.getDayText = this.getDayText.bind(this);
        this.getWeekDays = this.getWeekDays.bind(this);
        this.getColor = this.getColor.bind(this);
        this.getIcon = this.getIcon.bind(this);
    }

    componentWillMount() {
        this.getDersProgrami(new Date(), true);
    }

    getDersProgrami(currentDate, isSetCurrentDate) {
        let date = currentDate.getFullYear() + "-" + (currentDate.getMonth() + 1) + "-" + currentDate.getDate();
        this.webClient.get(`Mobile/StudioReservations/Programs/0/${date}/true`)
            .then(response => {
                this.setState({
                    currentDate: isSetCurrentDate ? currentDate : this.state.currentDate,
                    selectedDate: currentDate,
                    selectedDateProgram: response.data.data
                });
            });
    }

    getMonthText(date) {
        var months = ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül',
            'Ekim', 'Kasım', 'Aralık'];
        return months[date.getMonth()];
    }

    getDayText(date) {
        var day = ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"];
        return day[date.getDay()];
    }

    getWeekDays(startDate) {
        var aryDates = [];

        for (var i = 0; i < 7; i++) {
            var currentDate = new Date();
            currentDate.setDate(startDate.getDate() + i);
            aryDates.push(currentDate);
        }

        return aryDates;
    }

    getColor(status) {
        switch (status) {
            case 1:
                return "rgb(238,143,77)";
            case -1:
                return "rgb(186,215,0)";
            default:
                return "rgb(204, 204, 204)";
        }
    }

    getIcon(status) {
        switch (status) {
            case 1:
                return require("../../assets/beyazSagOk.png");
            case -1:
                return require("../../assets/beyazOk.png");
            default:
                return null;
        }
    }

    openProgramDetailPage(program) {
        this.webClient.get(`Mobile/StudioReservations/ProgramDetails/${program.programID}`)
            .then(response => {
                if (response.data.data.isSelectPlace) {
                    this.props.navigation.navigate("SelectPosition", response.data.data);
                }
                else {
                    this.props.navigation.navigate("SelectPackage", { program: response.data.data });
                }
            });
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%" }}>
                <View style={{
                    marginTop: 5, marginBottom: 5, flexDirection: "row",
                    justifyContent: "space-between", alignItems: "center"
                }}>
                    <Text style={{ marginLeft: 10, fontSize: 18, fontWeight: "400", color: "rgb(65,159,221)" }}>
                        {`${this.getMonthText(this.state.currentDate)} ${this.state.currentDate.getFullYear()}`}
                    </Text>
                    <TouchableOpacity onPress={() => { this.popupDialog.show() }}>
                        <Icon style={{ marginRight: 10, color: "rgb(65,159,221)" }} name="calendar" />
                    </TouchableOpacity>
                </View>

                <View style={{ marginTop: 5, marginBottom: 5 }}>
                    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                        {this.getWeekDays(this.state.currentDate).map((day, index) => {
                            return (
                                <TouchableOpacity key={index}
                                    style={{
                                        height: 120, width: 120, marginLeft: 5, marginRight: 5,
                                        justifyContent: "space-between", alignItems: "center",
                                        backgroundColor: day.getDate() == this.state.selectedDate.getDate() ? "rgb(246,204,70)" : "lightgray"
                                    }}
                                    onPress={() => this.getDersProgrami(new Date(day), false)}>
                                    <Text style={{ color: "white", marginTop: 10, fontSize: 18, fontWeight: "bold" }}>{this.getDayText(day)}</Text>
                                    {
                                        new Date().getDate() == day.getDate() ?
                                            <Text style={{ color: "white", fontSize: 12 }}>(Bugün)</Text> :
                                            <Text />
                                    }
                                    <Text style={{ color: "white", marginBottom: 10, fontSize: 48, fontWeight: "bold" }}>{day.getDate()}</Text>
                                </TouchableOpacity>
                            )
                        })}
                    </ScrollView>
                </View>

                <ScrollView style={{ marginTop: 5, marginBottom: 5, marginRight: 5 }} showsVerticalScrollIndicator={false}>
                    {this.state.selectedDateProgram.map((program, item) => {
                        return (
                            <TouchableOpacity style={{
                                margin: 5, width: "100%", height: 60,
                                justifyContent: "space-between", alignItems: "center", flexDirection: "row",
                                backgroundColor: this.getColor(program.result)
                            }}
                                onPress={() => this.openProgramDetailPage(program)}>
                                <View>
                                    <Text style={{ marginLeft: 5, color: "white", fontSize: 16, fontWeight: "bold" }}>
                                        {program.programStartFormatted + " / " + program.programFinishFormatted}
                                    </Text>
                                    <Text style={{ marginLeft: 5, marginTop: 5, color: "white", fontSize: 12, fontWeight: "bold" }}>
                                        {program.lessonName + " - " + program.trainerName}
                                    </Text>
                                </View>
                                <ImageBackground source={this.getIcon(program.result)}
                                    style={{ width: 35, height: 35, marginRight: 25 }} />
                            </TouchableOpacity>
                        )
                    })}
                </ScrollView>
                <PopupDialog ref={(popupDialog) => { this.popupDialog = popupDialog; }}>
                    <View style={{ width: "100%", height: 400 }}>
                        <Calendar current={this.state.selectedDate}
                            onDayPress={(day) => {
                                this.popupDialog.dismiss();
                                this.getDersProgrami(new Date(day.dateString), true);
                            }} />
                    </View>
                </PopupDialog>
            </View>
        );
    }
}
import React from "react";
import { View, ImageBackground } from 'react-native';
import { Header, Body, Title, Thumbnail } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { connect } from 'react-redux';

import headerSelector from '../redux/selectors/headerSelector'

@connect(headerSelector)
export default class MainHeaderBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            title: this.props.title,
            iconUrl: this.props.iconUrl
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            title: nextProps.title,
            iconUrl: nextProps.iconUrl
        });
    }

    render() {
        console.log(this.state);
        return (
            <Header style={{ backgroundColor: "rgb(226,225,225)" }} androidStatusBarColor="rgb(91,91,91)">
                <Body style={{ flex: 3, flexDirection: 'row', alignItems: "center" }}>
                    <Thumbnail small circular={true} style={{ backgroundColor: "transparent" }}
                        source={{ uri: this.state.iconUrl }} />
                    <Title style={{ fontWeight: "400", color: "black", marginLeft: 10 }}>{this.state.title}</Title>
                </Body>
            </Header >
        );
    }
}
import React, { Component } from 'react';
import { View, ImageBackground, WebView } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { Container, Header, Content, Tab, Tabs, TabHeading, Text, Button } from 'native-base';

import HeaderBar from '../HeaderBar';

const title = "Kullanım Koşulları";

export default class KullanimKosullari extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar isShowBack={false} navigation={navigation} title={title} />
    });

    constructor(props) {
        super(props);
        this.state = props.navigation.state.params;
        this._acceptRules = this._acceptRules.bind(this);
        this._rejectRules = this._rejectRules.bind(this);
    }

    _acceptRules() {
        this.state.accept();
        this.props.navigation.goBack();
    }

    _rejectRules() {
        this.state.reject();
        this.props.navigation.goBack();
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%" }}>
                <WebView style={{ flex: 1 }} html={this.state.doc} 
                scalesPageToFit={false} />
                <View style={{ flexDirection: "row" }}>
                    <Button full danger style={{ width: "50%" }} onPress={this._rejectRules}>
                        <Text>REDDET</Text>
                    </Button>
                    <Button full success style={{ width: "50%" }} onPress={this._acceptRules}>
                        <Text>KABUL ET</Text>
                    </Button>
                </View>
            </View>
        );
    }
}
import React from "react";
import { AppRegistry, ImageBackground, StatusBar, View, ScrollView, AsyncStorage } from "react-native";
import { Container, Content, Text, List, ListItem } from "native-base";
import { NavigationActions } from 'react-navigation';
import { Divider } from 'react-native-elements';
import RNExitApp from 'react-native-exit-app';

const routes = [
  { image: require("../assets/menu/dersProgramim.png"), text: "Ders Programı", width: 25, height: 34.5, navText: "DersProgramim" },
  { image: require("../assets/menu/mesajlarim.png"), text: "Mesajlarım", width: 25, height: 20, navText: "Mesajlarim" },
  { image: require("../assets/menu/sporProgramim.png"), text: "Spor Programım", width: 25, height: 34.5, navText: "SporProgramim" },
  { image: require("../assets/menu/paketlerim.png"), text: "Paketlerim", width: 25, height: 25, navText: "Paketlerim" },
  { image: require("../assets/menu/uyeliklerim.png"), text: "Üyeliklerim", width: 25, height: 25, navText: "Uyeliklerim" },
  { image: require("../assets/menu/rezarvasyonlarim.png"), text: "Rezervasyonlarım", width: 25, height: 27, navText: "Rezarvasyonlarim" },
  { image: require("../assets/menu/olculerim.png"), text: "Ölçümlerim", width: 25, height: 25, navText: "Olculerim" },
  { image: require("../assets/menu/kareKodIleGecis.png"), text: "Karekod oluştur", width: 25, height: 25, navText: "KareKodIleGecis" },
  { image: require("../assets/menu/sifreDegistir.png"), text: "Şifre değiştirme", width: 25, height: 25, navText: "SifreDegistirme" },
  { image: require("../assets/menu/hakkinda.png"), text: "Hakkında", width: 25, height: 25, navText: "Hakkinda" },
  { image: require("../assets/menu/cikis.png"), text: "Çıkış", width: 25, height: 25, navText: "Cikis" },
];

export default class SideBar extends React.Component {

  constructor(props) {
    super(props);
    this._navigate = this._navigate.bind(this);
  }

  _navigate(path) {
    if (path == "Cikis") {
      AsyncStorage.removeItem("login_status");

      AsyncStorage.getItem("login_status").then(function(value){
          //console.log(value);
      });        
      
      
      setTimeout(() => {
        RNExitApp.exitApp();  
      },2000);
      
      
    }
    else {
      const resetAction = NavigationActions.reset({
        index: 1,
        actions: [
          NavigationActions.navigate({ routeName: "Main", params: {stat: false} }),
          NavigationActions.navigate({ routeName: path })
        ]
      });
      this.props.navigation.dispatch(resetAction);
    }
  }

  render() {
    return (
      <Container>
        <View style={{ marginTop: 50 }}>
          <Text style={{ marginLeft: 20 }}>Ana Menü</Text>
          <Divider style={{ backgroundColor: 'black', width: "100%", height: 1, marginTop: 5 }} />
          <ScrollView style={{ height: "90%", marginLeft: 20, marginBottom: 20 }}>
            {routes.map((data, index) => {
              return (
                <ListItem key={index}
                  itemDivider
                  style={{ backgroundColor: "white" }}
                  onPress={() => this._navigate(data.navText)}>
                  <ImageBackground source={data.image}
                    style={{ width: data.width, height: data.height }} />
                  <Text style={{ marginLeft: 20 }}>{data.text}</Text>
                </ListItem>
              );
            })}
          </ScrollView>
        </View>
      </Container>
    );
  }
} 
import React, { Component } from 'react';
import { View, TouchableOpacity, ScrollView, ListView, Dimensions } from 'react-native';
import { Container, Header, Content, Tab, Tabs, TabHeading, Text, Button, List } from 'native-base';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';


import WebClient from '../../helper/webclient'
import HeaderBar from '../HeaderBar'
import RezervasyonItem from './components/rezervasyonItem'

const title = "Rezervasyonlarım";

export default class Index extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor() {
        super();
        this.webClient = new WebClient();

        this.state = {
            bireyselRezervasyonlarim: [],
            studyoRezervasyonlarim: [],
            isShowPast: false
        }

        this._setRezervasyonlarim = this._setRezervasyonlarim.bind(this);
        this._getActiveRezervations = this._getActiveRezervations.bind(this);
        this._getDeactiveRezervations = this._getDeactiveRezervations.bind(this);
        this._rezervasyonIptal = this._rezervasyonIptal.bind(this);
        this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    }

    componentWillMount() {
        setTimeout(() => {
            this._setRezervasyonlarim();
        }, 500);
    }

    _getActiveRezervations() {
         let promise1 = this.webClient.get(`Mobile/IndividualReservations/1`)
             .then(response => {
                 this.setState({ bireyselRezervasyonlarim: response.data.data });

                let promise2 = this.webClient.get(`Mobile/StudioReservations/1`)
                    .then(response2 => {
                        this.setState({ studyoRezervasyonlarim: response2.data.data });
                    });
             });
    }

    _getDeactiveRezervations() {
         let promise1 = this.webClient.get(`Mobile/IndividualReservations/0`)
             .then(response => {
                 this.setState({ bireyselRezervasyonlarim: response.data.data });

                let promise2 = this.webClient.get(`Mobile/StudioReservations/0`)
                    .then(response2 => {
                        this.setState({ studyoRezervasyonlarim: response2.data.data });
                    });
             });
    }

    _setRezervasyonlarim() {
        if (this.state.isShowPast) {
            this._getDeactiveRezervations();
        }
        else {
            this._getActiveRezervations();
        }
    }

    _rezervasyonIptal(rezervasyonType, data) {
        if (rezervasyonType == "bireyselRezervasyon") {
            this.webClient.get(`Mobile/IndividualReservations/Cancel/${data.reservationID}`)
                .then(response => {
                    this._setRezervasyonlarim();
                });
        }
        else {
            this.webClient.get(`Mobile/StudioReservations/Cancel/${data.reservationID}`)
                .then(response => {
                    this._setRezervasyonlarim();
                });
        }
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%", }}>
                <Tabs locked={true} initialPage={0} tabBarUnderlineStyle={{ backgroundColor: "rgb(100,100,100)" }}>
                    <Tab heading={<TabHeading style={{ backgroundColor: "rbg(248,248,248)" }}><Text style={{ color: "rgb(111,111,111)", fontSize: 14 }}>BİREYSEL</Text></TabHeading>}>
                        <ScrollView>
                            <List>
                                {
                                    this.state.bireyselRezervasyonlarim.map(item =>
                                        <RezervasyonItem {...item} isShowIptal={!this.state.isShowPast}
                                            rezervasyonType="bireyselRezervasyon"
                                            onPressIptal={() => this._rezervasyonIptal("bireyselRezervasyon", item)} />)
                                }
                            </List>
                        </ScrollView>
                    </Tab>
                    <Tab heading={<TabHeading style={{ backgroundColor: "rbg(248,248,248)" }}><Text style={{ color: "rgb(111,111,111)", fontSize: 14 }}>STÜDYO</Text></TabHeading>}>
                        <ScrollView>
                            <List>
                                {
                                    this.state.studyoRezervasyonlarim.map(item =>
                                        <RezervasyonItem {...item} isShowIptal={!this.state.isShowPast}
                                            rezervasyonType="studyoRezervasyon"
                                            onPressIptal={() => this._rezervasyonIptal("studyoRezervasyon", item)} />)
                                }
                            </List>
                        </ScrollView>
                    </Tab>
                </Tabs>
                <View style={{ flexDirection: "row", position: "relative", bottom: 0 }}>
                    <Button full info style={{ width: "100%" }}
                        onPress={() => this.setState({ isShowPast: !this.state.isShowPast, studyoRezervasyonlarim: [], bireyselRezervasyonlarim: [] },
                            this._setRezervasyonlarim)}>
                        {
                            this.state.isShowPast ?
                                <Text>AKTİFLERİ GÖSTER</Text> :
                                <Text>GEÇMİŞİ GÖSTER</Text>
                        }
                    </Button>
                </View>
            </View>
        );
    }
}
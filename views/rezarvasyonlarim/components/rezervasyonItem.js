import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { ListItem, Left, Body, Right, Text, Button } from 'native-base';

export default class RezervasyonItem extends Component {

    constructor(props) {
        super(props);

        this._getStatusText = this._getStatusText.bind(this);
        this._formatDate = this._formatDate.bind(this);
    }

    _getStatusText(statusCode) {
        switch (statusCode) {
            case -1:
                return "İptal Edildi";
            case 1:
                return "Ön Onaylı";
            case 2:
                return "Onaylı";
            case 3:
                return "Tesiste";
            case 4:
                return "Cezalı Onaylı";
            default:
                return "";
        }
    }

    _formatDate(date) {
        let realDate = new Date(date);
        return ("00" + realDate.getDate()).slice(-2) + "-" + ("00" + (realDate.getMonth() + 1)).slice(-2) + "-" + realDate.getFullYear();
    }

    render() {
        return (
            <ListItem last>
                <Body style={{ flexDirection: "row", alignItems: "space-between", alignItems: "center", position: "relative" }}>
                    {
                        this.props.rezervasyonType == "bireyselRezervasyon" ?
                            <View>
                                <Text style={{ fontSize: 14 }}>{this.props.serviceName}</Text>
                                <Text style={{ fontSize: 14 }}>{this._formatDate(this.props.reservationDate)}, {this.props.startTimeFormatted} / {this.props.endTimeFormatted}</Text>
                                <Text style={{ fontSize: 12 }} note>{this.props.resourceName}</Text>
                            </View> :
                            <View>
                                <Text style={{ fontSize: 14 }}>{this.props.serviceName}</Text>
                                <Text style={{ fontSize: 14 }}>{this._formatDate(this.props.reservationDate)}, {this.props.startTimeFormatted} / {this.props.endTimeFormatted}</Text>
                                <Text style={{ fontSize: 12 }} note>{this.props.studioName}, {this.props.trainerName}, {this.props.placeNo}</Text>
                            </View>
                    }
                </Body>
                <Right style={{ alignItems: "center" }}>
                    {
                        this.props.isShowIptal ?
                            <Button style={{
                                width: 70, height: 35, borderRadius: 5,
                                backgroundColor: "rgb(236, 48, 35)", marginBottom: 5
                            }}
                                onPress={() => this.props.onPressIptal()}>
                                <Text style={{
                                    width: "100%", fontSize: 14,
                                    paddingLeft: 0, paddingRight: 0, textAlign: "center"
                                }}>İptal Et</Text>
                            </Button> :
                            null
                    }
                    <Text style={{ fontSize: 14 }} note>{this._getStatusText(this.props.statusCode)}</Text>
                </Right>
            </ListItem>
        );
    }
}

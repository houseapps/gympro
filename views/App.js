import React, { Component } from 'react';
import { View, ImageBackground, TouchableOpacity } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Label } from 'native-base';
import { DrawerNavigator, TabNavigator, StackNavigator, NavigationActions } from "react-navigation";
import { Divider } from 'react-native-elements';
import { Col, Row, Grid } from 'react-native-easy-grid';

import SideBar from './SideBar';
import FooterBar from "./FooterBar";

import Main from './Main';
import Info from './Info';

import KullanimKosullari from './common/kullanimKosullari';

import DersProgramim from './dersProgramim';
import SelectPosition from './dersProgramim/views/selectPosition';
import SelectPackage from './dersProgramim/views/selectPackage';
import Bilgilendirme from './dersProgramim/views/bilgilendirme';

import KareKodIleGecis from './kareKodIleGecis';
import Mesajlarim from './mesajlarim';
import MesajGonder from './mesajlarim/views/mesajGonder';
import MesajYanitla from './mesajlarim/views/mesajGonder';
import Olculerim from './olculerim';
import Paketlerim from './paketlerim';

import SatinAl from './satinAl';
import SatinAlSummary from './satinAl/views/summary';
import SatinAlBilgilendirme from './satinAl/views/bilgilendirme';

import Rezarvasyonlarim from './rezarvasyonlarim';

import HemenRezarvasyonlarim from './hemenRezervasyon';
import BireyselHemenRezarvasyonlarim from './hemenRezervasyon/views/bireyselHemenRezervasyon';

import SporProgramim from './sporProgramim';
import HaftalikPlanimDetay from './sporProgramim/views/haftalikPlanimDetay';
import CalismaPlanim from './sporProgramim/views/calismaPlanim';
import ProgramlarimCalismaPlanim from './sporProgramim/views/programlarimCalismaPlanim';

import SifreDegistirme from './sifreDegistirme';
import Hakkinda from './hakkinda';

import Uyeliklerim from './uyeliklerim';

import * as AppActions from '../redux/actions/app';

const navigateOnce = (getStateForAction) => (action, state) => {
    AppActions.isLoading();
    const { type, routeName } = action;
    let result = (
        state &&
        type === NavigationActions.NAVIGATE &&
        routeName === state.routes[state.routes.length - 1].routeName
    ) ? null : getStateForAction(action, state);
    AppActions.isLoaded();
    return result;
};

const StackNavigationForMain = StackNavigator(
  {
    Main: { screen: Main },
    Info: { screen: Info },
    KullanimKosullari: { screen: KullanimKosullari },
    DersProgramim: { screen: DersProgramim },
    SelectPosition: { screen: SelectPosition },
    SelectPackage: { screen: SelectPackage },
    Bilgilendirme: { screen: Bilgilendirme },
    KareKodIleGecis: { screen: KareKodIleGecis },
    Mesajlarim: { screen: Mesajlarim },
    MesajGonder: { screen: MesajGonder },
    MesajYanitla: { screen: MesajYanitla },
    Olculerim: { screen: Olculerim },
    Paketlerim: { screen: Paketlerim },
    SatinAl: { screen: SatinAl },
    SatinAlSummary: { screen: SatinAlSummary },
    SatinAlBilgilendirme: { screen: SatinAlBilgilendirme },
    Rezarvasyonlarim: { screen: Rezarvasyonlarim },
    HemenRezarvasyonlarim: { screen: HemenRezarvasyonlarim },
    BireyselHemenRezarvasyonlarim: { screen: BireyselHemenRezarvasyonlarim },
    StudyoHemenRezervasyonlarim: { screen: DersProgramim },
    SporProgramim: { screen: SporProgramim },
    HaftalikPlanimDetay: { screen: HaftalikPlanimDetay },
    CalismaPlanim: { screen: CalismaPlanim },
    ProgramlarimCalismaPlanim: { screen: ProgramlarimCalismaPlanim },
    Uyeliklerim: { screen: Uyeliklerim },
    SifreDegistirme: { screen: SifreDegistirme },
    Hakkinda: { screen: Hakkinda },
  }, {
    headerMode: 'screen',
    mode: "card",
    cardStyle: {
      backgroundColor: "white"
    }
  }
);
StackNavigationForMain.router.getStateForAction = navigateOnce(StackNavigationForMain.router.getStateForAction);

const StackNavigationForInfo = StackNavigator(
  {
    Info: { screen: Info },
    Main: { screen: Main },
    KullanimKosullari: { screen: KullanimKosullari },
    DersProgramim: { screen: DersProgramim },
    SelectPosition: { screen: SelectPosition },
    SelectPackage: { screen: SelectPackage },
    Bilgilendirme: { screen: Bilgilendirme },
    KareKodIleGecis: { screen: KareKodIleGecis },
    Mesajlarim: { screen: Mesajlarim },
    MesajGonder: { screen: MesajGonder },
    MesajYanitla: { screen: MesajYanitla },
    Olculerim: { screen: Olculerim },
    Paketlerim: { screen: Paketlerim },
    SatinAl: { screen: SatinAl },
    SatinAlSummary: { screen: SatinAlSummary },
    SatinAlBilgilendirme: { screen: SatinAlBilgilendirme },
    Rezarvasyonlarim: { screen: Rezarvasyonlarim },
    HemenRezarvasyonlarim: { screen: HemenRezarvasyonlarim },
    BireyselHemenRezarvasyonlarim: { screen: BireyselHemenRezarvasyonlarim },
    StudyoHemenRezervasyonlarim: { screen: DersProgramim },
    SporProgramim: { screen: SporProgramim },
    HaftalikPlanimDetay: { screen: HaftalikPlanimDetay },
    CalismaPlanim: { screen: CalismaPlanim },
    ProgramlarimCalismaPlanim: { screen: ProgramlarimCalismaPlanim },
    Uyeliklerim: { screen: Uyeliklerim },
    SifreDegistirme: { screen: SifreDegistirme },
    Hakkinda: { screen: Hakkinda },
  }, {
    headerMode: 'screen',
    mode: "card",
    cardStyle: {
      backgroundColor: "white"
    }
  }
);
StackNavigationForInfo.router.getStateForAction = navigateOnce(StackNavigationForInfo.router.getStateForAction);

const TabNavigation = TabNavigator(
  {
    Main: { screen: StackNavigationForMain },
    Info: { screen: StackNavigationForInfo },
  },
  {
    tabBarPosition: "bottom",
    swipeEnabled: false,
    tabBarComponent: props => {
      return <FooterBar navigation={props.navigation} />;
    },
    cardStyle: {
      backgroundColor: "white"
    }
  }
);
TabNavigation.router.getStateForAction = navigateOnce(TabNavigation.router.getStateForAction);

const App = DrawerNavigator(
  {
    App: {
      screen: TabNavigation
    }
  },
  {
    contentComponent: props => <SideBar navigation={props.navigation} />,
    cardStyle: {
      backgroundColor: "white"
    }
  }
);
App.router.getStateForAction = navigateOnce(App.router.getStateForAction);

export default App 
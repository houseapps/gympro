import React, { Component } from 'react';
import { View, ScrollView, TouchableOpacity } from 'react-native';
import { Tab, Tabs, TabHeading, List, ListItem, Left, Body, Right, Thumbnail, Text, Button } from 'native-base';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';

import HeaderBar from '../HeaderBar';
import GelenMesajlar from './views/gelenMesajlar';
import GidenMesajlar from './views/gidenMesajlar';

const title = "Mesajlarım";

export default class Index extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor() {
        super();
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%" }}>
                <Tabs locked={true} initialPage={0} tabBarUnderlineStyle={{ backgroundColor: "rgb(100,100,100)" }}>
                    <Tab heading={<TabHeading style={{ backgroundColor: "rbg(248,248,248)" }}><Text style={{ color: "rgb(111,111,111)", fontSize: 14 }}>GELEN MESAJLAR</Text></TabHeading>}>
                        <GelenMesajlar navigation={this.props.navigation} />
                    </Tab>

                    <Tab heading={<TabHeading style={{ backgroundColor: "rbg(248,248,248)" }}><Text style={{ color: "rgb(111,111,111)", fontSize: 14 }}>GİDEN MESAJLAR</Text></TabHeading>}>
                        <GidenMesajlar navigation={this.props.navigation} />
                    </Tab>
                </Tabs>
                <View style={{ flexDirection: "row" }}>
                    <Button full info style={{ width: "100%" }}
                        onPress={() => this.props.navigation.navigate("MesajGonder", { type: "YeniMesaj" })}>
                        <Text>YENİ MESAJ YAZ</Text>
                    </Button>
                </View>
            </View>
        );
    }
}
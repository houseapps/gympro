import React, { Component } from 'react';
import { View, TouchableOpacity, ScrollView, ListView, Dimensions } from 'react-native';
import { List, ListItem, Left, Body, Right, Thumbnail, Text } from 'native-base';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';

import WebClient from '../../../helper/webclient';

export default class GidenMesajlar extends Component {
    constructor(props) {
        super(props);
        this.webClient = new WebClient();

        this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            data: []
        }

        this._deleteMessage = this._deleteMessage.bind(this);
    }

    componentWillMount() {
        this.webClient.get(`Mobile/Messages/Outbox`)
            .then(response => {
                this.setState({
                    data: response.data.data
                });
            });
    }

    _deleteMessage(message, secId, rowId, rowMap) {
        rowMap[`${secId}${rowId}`].props.closeRow();
        this.webClient.delete(`Mobile/Messages/DeleteMessage/${message.messageID}`)
            .then(response => {
                this.webClient.get(`Mobile/Messages/Outbox`)
                    .then(response => {
                        this.setState({
                            data: response.data.data
                        });
                    });
            });
    }

    render() {

        return (
            <List
                dataSource={this.ds.cloneWithRows(this.state.data)}
                renderRow={item =>
                    <ListItem
                        onPress={() => this.props.navigation.navigate("MesajGonder", { type: "MesajOkuGiden", postIsReaded: false, ...item, cb: this.componentWillMount.bind(this) })}>
                        <Thumbnail source={{ uri: item.avatarPath }} />
                        <Body>
                            <View>
                                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>
                                    <Text style={{ marginTop: 2, fontSize: 14, fontWeight: "normal"}}>
                                        {item.recipientName}({item.gymProPersonalName == "" ? item.recipientName : item.gymProPersonalName})
                                    </Text>
                                </View>
                                <View>
                                    <Text style={{ marginTop: 2, fontSize: 12, fontWeight: "normal"}}>{item.subject}</Text>
                                    <Text style={{ marginTop: 2, fontSize: 12 }} Fnote>{item.messageContent}</Text>
                                </View>
                            </View>
                        </Body>
                        <Right>
                            <Text style={{ marginTop: 2, fontSize: 12 }} note>{new Date(item.creationDate).toLocaleDateString("tr-TR")}</Text>
                        </Right>
                    </ListItem>
                }
                disableRightSwipe
                renderLeftHiddenRow={(data, secId, rowId, rowMap) => { }}
                renderRightHiddenRow={(data, secId, rowId, rowMap) =>
                    <TouchableOpacity onPress={() => this._deleteMessage(data, secId, rowId, rowMap)}
                        style={{ width: "100%", height: "100%", backgroundColor: "red", alignItems: "center", justifyContent: "center" }}>
                        <Icon style={{ color: "white", fontSize: 28 }} name="delete" />
                    </TouchableOpacity>}
                rightOpenValue={Dimensions.get('window').width / 4 * -1}
            />
        );
    }
}
import React, { Component } from 'react';
import { View, ScrollView, TouchableOpacity } from 'react-native';
import { Form, Label, Input, Picker, Item, Text, Icon, Button } from 'native-base';
import { GiftedChat } from 'react-native-gifted-chat';
import { NavigationActions } from 'react-navigation';

import HeaderBar from '../../HeaderBar';
import WebClient from '../../../helper/webclient';

const title = "Mesaj";

export default class MesajGonder extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} cb={navigation.state.params.cb} />
    });

    constructor(props) {
        super(props);
        var message = {
            ParentMessageID: 0,
            Subject: "",
            MessageContent: "",
            GymProPersonalId: undefined,
            GymProPersonalName: undefined,
            SenderIP: "127.0.0.1"
        };
        var sender = -1;

        var parentMessage = props.navigation.state.params;
        if (parentMessage) {
            if (parentMessage.type == "SporProgramim") {
                sender = parentMessage.fitnessConsultantID;
                message = {
                    ...message,
                    GymProPersonalId: parentMessage.fitnessConsultantID,
                    GymProPersonalName: parentMessage.fitnessConsultantName + " " + parentMessage.fitnessConsultantSurName
                }
            }
            else {
                sender = parentMessage.gymProPersonalID;
                message = {
                    ...message,
                    ParentMessageID: parentMessage.messageID,
                    Subject: parentMessage.subject,
                    MessageContent: parentMessage.type.includes("MesajOku") ? parentMessage.messageContent : "",
                    GymProPersonalId: parentMessage.gymProPersonalID,
                    GymProPersonalName: parentMessage.gymProPersonalName
                }
            }
        }

        this.state = {
            sender: sender,
            senderList: [],
            parentMessage: parentMessage,
            message: message
        };

        this.webClient = new WebClient();
        this._sendMessage = this._sendMessage.bind(this);
    }

    componentWillMount() {
        this.webClient.get(`Mobile/Messages/Recipients`)
            .then(response => {
                let senderList = response.data.data;
                if (this.state.parentMessage && (this.state.parentMessage.type == "SporProgramim" || this.state.parentMessage.type.includes("MesajOku"))) {
                    senderList = senderList;
                }
                else {
                    senderList = senderList.filter(x => x.useNewMessage);
                }
                this.setState({
                    senderList: senderList
                });
            });
        if (this.state.parentMessage && this.state.parentMessage.type.includes("MesajOku") && this.state.parentMessage.postIsReaded && !this.state.parentMessage.isRead) {
            this.webClient.put(`Mobile/Messages/ReadMessage/${this.state.parentMessage.messageID}`);
        }
    }

    _sendMessage() {
        this.webClient.post(`Mobile/Messages/SendMessage`, this.state.message)
            .then(response => {

                const resetAction = NavigationActions.reset({
                    index: 1,
                    actions: [
                        NavigationActions.navigate({ routeName: "Main" }),
                        NavigationActions.navigate({ routeName: "Mesajlarim" })
                    ]
                });
                this.props.navigation.dispatch(resetAction);
            });
    }

    render() {
        return (
            <Form style={{ width: "100%", height: "100%" }}>
                <Item style={{ marginLeft: 5 }}>
                    <Label>{this.state.parentMessage.type.includes("MesajOkuGelen") ? "Kimden" : "Kime"} : </Label>
                    {
                        this.state.parentMessage.type.includes("MesajOku") || this.state.parentMessage.type == "GelenMesajlar" || this.state.parentMessage.type == "SporProgramim" ?
                            <Input
                                style={{ width: "100%", height: 40, color: "black" }}
                                disabled
                                value={this.state.senderList.length == 0 ? "" : this.state.senderList.filter(x => x.staffID == this.state.sender)[0].staffName} /> :
                            <Picker
                                style={{ width: "100%", height: 40, color: "black" }}
                                textStyle={{ color: 'black' }}
                                iosHeader="Seçim yapınız"
                                headerBackButtonText="Geri"
                                mode="dropdown"
                                placeholder="Mesaj Gönderilecek Kişiyi Seçiniz..."
                                selectedValue={this.state.sender}
                                onValueChange={(sender) => {
                                    var person = this.state.senderList.filter(x => x.staffID == sender)[0];
                                    this.setState({
                                        sender: sender,
                                        message: {
                                            ParentMessageID: this.state.message.ParentMessageID,
                                            Subject: this.state.message.Subject,
                                            MessageContent: this.state.message.MessageContent,
                                            GymProPersonalId: person.consultantID,
                                            GymProPersonalName: person.screenName
                                        }
                                    })
                                }}>
                                {
                                    this.state.senderList.map((item, index) => <Item key={item.staffID} value={item.staffID} label={item.staffName} />)
                                }
                            </Picker>
                    }
                </Item>
                <Item style={{ marginLeft: 5 }} inlineLabel>
                    <Label>Mesaj Konusu : </Label>
                    <Input
                        disabled={this.state.parentMessage.type.includes("MesajOku") || this.state.parentMessage.type == "GelenMesajlar"}
                        value={this.state.message.Subject}
                        onChangeText={(value) => {
                            let newMessage = { ...this.state.message, Subject: value };
                            this.setState({ message: newMessage });
                        }} />
                </Item>
                <Input style={{ flex: 1, textAlignVertical: "top" }} placeholder="Mesajınızı buraya yazınız"
                    blurOnSubmit={true}
                    multiline={true}
                    disabled={this.state.parentMessage.type.includes("MesajOku")}
                    value={this.state.message.MessageContent}
                    onChangeText={(value) => {
                        let newMessage = { ...this.state.message, MessageContent: value };
                        this.setState({ message: newMessage });
                    }} />
                {
                    this.state.parentMessage != undefined ?
                        (
                            this.state.parentMessage.type.includes("MesajOku") ?
                                (
                                    this.state.parentMessage.type == "MesajOkuGelen" ?
                                        <View style={{ flexDirection: "row", position: "relative", bottom: 0 }}>
                                            <Button full info style={{ width: "100%" }}
                                                onPress={() => this.props.navigation.navigate("MesajYanitla", { ...this.state.parentMessage, type: "GelenMesajlar", messageContent: "" })}>
                                                <Text>YANITLA</Text>
                                            </Button>
                                        </View> : null
                                )
                                :
                                <View style={{ flexDirection: "row", position: "relative", bottom: 0 }}>
                                    <Button full info style={{ width: "100%" }}
                                        onPress={() => this._sendMessage()}>
                                        <Text>GÖNDER</Text>
                                    </Button>
                                </View>
                        ) :
                        null
                }
            </Form>
        );
    }
}
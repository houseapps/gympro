import React from "react";
import { View, ImageBackground, TouchableOpacity, Platform } from 'react-native';
import { Header, Body, Title, Text, } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { NavigationActions } from 'react-navigation';

export default class HeaderBar extends React.Component {

    static defaultProps = {
        isShowBack: true
    }

    constructor(props) {
        super(props);
        this._goBackCb = this._goBackCb.bind(this);
    }

    _goBackCb() {
        if (this.props.cb) {
            this.props.cb();
        }
        this.props.navigation.dispatch(NavigationActions.back({ key: null }));
    }

    render() {
        return (
            <View style={{ backgroundColor: Platform.OS == "ios" ? "rgb(226,225,225)" : "rgb(91,91,91)" }}>
                <Header style={{ backgroundColor: "rgb(226,225,225)", marginTop: 5 }} androidStatusBarColor="rgb(91,91,91)">
                    <Body style={{ flex: 3, flexDirection: 'row', height: "100%", marginLeft: -10, marginRight: -10 }}>
                        <Grid style={{ width: "100%", height: "100%" }}>
                            {
                                this.props.isShowBack ?
                                    <Col style={{ justifyContent: "center", backgroundColor: "rgb(101,101,101)", width: 75 }}>
                                        <TouchableOpacity
                                            onPress={this._goBackCb}>
                                            <ImageBackground source={require("../assets/beyazSolOk.png")}
                                                style={{ width: 35, height: 25, marginLeft: 15 }} />
                                        </TouchableOpacity>
                                    </Col> : null
                            }
                            <Col style={{ justifyContent: "center", backgroundColor: "rgb(195,197,196)" }}>
                                <Title style={{ fontWeight: "400", color: "black", position: "absolute", right: 10 }}>{this.props.title}</Title>
                            </Col>
                        </Grid>
                    </Body>
                </Header>
            </View>
        );
    }
}
import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import { Tab, Tabs, TabHeading, Text, Button } from 'native-base';
import { Divider } from 'react-native-elements';

import HeaderBar from '../HeaderBar';
import WebClient from '../../helper/webclient'
import PaketOzetItem from './components/paketOzetItem';

const title = "Paketlerim";

export default class Index extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor() {
        super();
        this.webClient = new WebClient();

        this.state = {
            pasifPaketler: [],
            aktifPaketler: []
        }
    }

    componentWillMount() {
        setTimeout(() => {
            this.webClient.get(`Mobile/Packages/1`)
                .then(response => {
                    this.setState({
                        aktifPaketler: response.data.data
                    });

                    this.webClient.get(`Mobile/Packages/0`)
                        .then(response2 => {
                            this.setState({
                                pasifPaketler: response2.data.data
                            });
                        });
            });
        }, 500);
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%", }}>
                <Tabs initialPage={0} tabBarUnderlineStyle={{ backgroundColor: "rgb(100,100,100)" }}>
                    <Tab heading={<TabHeading style={{ backgroundColor: "rbg(248,248,248)" }}><Text style={{ color: "rgb(111,111,111)", fontSize: 14 }}>AKTİF PAKETLERİM</Text></TabHeading>}>
                        <ScrollView style={{ width: "100%", height: "100%" }} showsVerticalScrollIndicator={false}>
                            {
                                this.state.aktifPaketler.map((item, index) =>
                                    <View key={index}>
                                        <PaketOzetItem {...item} />
                                        <Divider style={{ backgroundColor: 'rgb(158,158,158)', height: 1, width: "100%" }} />
                                    </View>
                                )
                            }
                        </ScrollView>
                    </Tab>

                    <Tab heading={<TabHeading style={{ backgroundColor: "rbg(248,248,248)" }}><Text style={{ color: "rgb(111,111,111)", fontSize: 14 }}>ESKİ PAKETLERİM</Text></TabHeading>}>
                        <ScrollView style={{ width: "100%", height: "100%" }} showsVerticalScrollIndicator={false}>
                            {
                                this.state.pasifPaketler.map((item, index) =>
                                    <View key={index}>
                                        <PaketOzetItem {...item} />
                                        <Divider style={{ backgroundColor: 'rgb(158,158,158)', height: 1, width: "100%" }} />
                                    </View>
                                )
                            }
                        </ScrollView>
                    </Tab>
                </Tabs>
                <View style={{ flexDirection: "row", position: "relative", bottom: 0 }}>
                    <Button full info style={{ width: "100%" }}
                        onPress={() => this.props.navigation.navigate("SatinAl")}>
                        <Text>YENİ PAKET</Text>
                    </Button>
                </View>
            </View>
        );
    }
}
import React, { Component } from 'react';
import { View } from 'react-native';

import { Text } from 'native-base'
import { Col, Row, Grid } from 'react-native-easy-grid';
import ProgressCircle from 'react-native-progress-circle';

export default class PaketOzetItem extends Component {
    constructor() {
        super();
    }

    render() {
        var kalanPercent = this.props.remainingCredit * 100 / this.props.totalCredit;
        return (
            <View style={{ flexDirection: "column", alignItems: "center", height: 175 }}>
                <View style={{ margin: 10 }}>
                    <Text style={{ fontSize: 14, fontWeight: "bold" }}>{this.props.packageName}</Text>
                </View>
                <View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ margin: 5 }}>
                            <ProgressCircle
                                borderWidth={15}
                                radius={60}
                                percent={100 - kalanPercent}
                                shadowColor="rgb(110,180,216)"
                                color="rgb(246,204,70)"
                                innerColor="white"
                                bgColor="white">
                                <Text>Kalan: {this.props.remainingCredit}</Text>
                            </ProgressCircle>
                        </View>
                        <View style={{ margin: 5 }}>
                            <View style={{ flex: 1, flexDirection: "column" }}>
                                <View style={{ margin: 5 }}>
                                    <Text style={{ fontWeight: "bold", fontSize: 12 }}>Başlangıç: {this.props.beginningDateFormatted}</Text>
                                    <Text style={{ fontWeight: "bold", fontSize: 12 }}>Bitiş: {this.props.endDateFormatted}</Text>
                                    <Text style={{ fontWeight: "bold", fontSize: 12 }}>Fiyat: {this.props.packageAmountFormatted}</Text>
                                </View>
                                <View style={{ margin: 5 }}>
                                    <Text style={{ fontWeight: "normal", fontSize: 12 }}>Toplam Seans: {this.props.totalCredit}</Text>
                                    <Text style={{ fontWeight: "normal", fontSize: 12 }}>Kullanılan Seans: {this.props.usedCredit}</Text>
                                    <Text style={{ fontWeight: "normal", fontSize: 12 }}>Kalan Seans: {this.props.remainingCredit}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}
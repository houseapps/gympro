import React, { Component } from 'react';
import { View, ScrollView, Alert } from 'react-native';
import { Form, Item, Input, Label, Button, Text } from 'native-base';

import HeaderBar from '../HeaderBar';
import WebClient from '../../helper/webclient';

const title = "Şifre Değiştirme";

export default class Index extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor() {
        super();
        this.state = {
            eskiSifre: "",
            yeniSifre: "",
            yeniSifreTekrar: ""
        }

        this.webClient = new WebClient();
        this._changePassword = this._changePassword.bind(this);
    }

    _changePassword() {
        this.webClient.post(`Mobile/Password/Change`, {
            OldPassword: this.state.eskiSifre,
            NewPassword: this.state.yeniSifre,
            NewPasswordRepeat: this.state.yeniSifreTekrar

        }, true, false)
            .then(response => {
                Alert.alert(
                    'Bilgi',
                    'Şifreniz başarıyla değiştirilmiştir.',
                    [
                        { text: 'Tamam', onPress: () => this.props.navigation.goBack() }
                    ],
                    { cancelable: false }
                )
            });
    }

    render() {
        return (
            <Form style={{ width: "100%", height: "100%" }}>
                <Item floatingLabel last>
                    <Label>Eski Şifre</Label>
                    <Input secureTextEntry
                        value={this.state.eskiSifre}
                        onChangeText={(text) => this.setState({ eskiSifre: text })} />
                </Item>
                <Item floatingLabel last>
                    <Label>Yeni Şifre</Label>
                    <Input secureTextEntry
                        value={this.state.yeniSifre}
                        onChangeText={(text) => this.setState({ yeniSifre: text })} />
                </Item>
                <Item floatingLabel last>
                    <Label>Yeni Şifre(Tekrar)</Label>
                    <Input secureTextEntry
                        value={this.state.yeniSifreTekrar}
                        onChangeText={(text) => this.setState({ yeniSifreTekrar: text })} />
                </Item>
                <View style={{ flex: 1 }} />
                <View style={{ flexDirection: "row", position: "relative", bottom: 0 }}>
                    <Button full info style={{ width: "100%" }}
                        onPress={this._changePassword}>
                        <Text>DEĞİŞTİR</Text>
                    </Button>
                </View>
            </Form>
        );
    }
}
import React, { Component } from 'react';
import { View, ImageBackground, Dimensions, ActivityIndicator } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Label } from 'native-base';
import { Divider } from 'react-native-elements';
import Swiper from 'react-native-swiper';
import WebClient from '../helper/webclient';

import MainHeaderBar from './MainHeaderBar';

export default class Info extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <MainHeaderBar />
    });

    constructor(props) {
        super(props);
        this.state = { data: [], showIndicator: true };

        this.webClient = new WebClient();
        this._getStyle = this._getStyle.bind(this);
    }

    componentWillMount() {
        this.webClient.get(`Mobile/MemberSummaryInformation`)
            .then(response => {
                this.setState({
                    data: response.data.data,
                    showIndicator: false
                });
            });
    }

    _getStyle(index) {
        switch (index % 6) {
            case 0:
                return styles.slide1;
            case 1:
                return styles.slide2;
            case 2:
                return styles.slide3;
            case 3:
                return styles.slide4;
            case 4:
                return styles.slide5;
            case 5:
                return styles.slide6;
        }
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%"}}>
            {
            this.state.showIndicator ?
            <ActivityIndicator size="large" color="#FFF" />
            :
            <Swiper
                onMomentumScrollEnd={(e, state, context) => console.log('index:', state.index)}
                dot={<View style={{backgroundColor: 'rgba(255,255,255,.4)', width: 5, height: 5, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3}} />}
                activeDot={<View style={{backgroundColor: '#FFF', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3}} />}
                loop={false}>
                {
                    this.state.data.map((item, index) =>
                        <View key={index} style={this._getStyle(index)}>
                            <Text style={styles.textHeader}>{item.value}</Text>
                            <Text style={styles.textBody}>{item.description}</Text>
                            <View style={{ flex: 1, flexDirection: 'row', margin: 20, position: "absolute", bottom: 80 }}>
                                <ImageBackground source={require("../assets/beyazSolOk.png")}
                                    style={{ width: 70, height: 50, marginLeft: 20, marginRight: 20 }} />
                                <ImageBackground source={require("../assets/beyazEl.png")}
                                    style={{ width: 50, height: 50 }} />
                                <ImageBackground source={require("../assets/beyazSagOk.png")}
                                    style={{ width: 70, height: 50, marginLeft: 20, marginRight: 20 }} />
                            </View>
                        </View>
                    )
                }
            </Swiper>
            }
            </View>
        );
    }
}

var styles = {
    slide1: {
        flex: 1,
        marginTop: -150,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(187,80,53)'
    },
    slide2: {
        flex: 1,
        marginTop: -150,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(224,131,65)'
    },
    slide3: {
        flex: 1,
        marginTop: -150,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(235,170,96)'
    },
    slide4: {
        flex: 1,
        marginTop: -150,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(224,131,65)'
    },
    slide5: {
        flex: 1,
        marginTop: -150,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(187,80,53)'
    },
    slide6: {
        flex: 1,
        marginTop: -150,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(235,170,96)'
    },
    textHeader: {
        color: '#fff',
        fontSize: 50,
        textAlign: "center",
        fontWeight: '600',
        marginLeft: 20,
        marginRight: 20,
    },
    textBody: {
        color: '#fff',
        fontSize: 30,
        textAlign: "center",
        fontWeight: '400',
        marginLeft: 20,
        marginRight: 20,
    }
}

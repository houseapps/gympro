import React, { Component } from 'react';
import { View, TouchableOpacity, ImageBackground, ScrollView } from 'react-native';
import { Text, Textarea, Icon } from 'native-base';

import webClient from '../../../helper/webclient';

export default class CalismaPlanimItem extends Component {

    constructor(props) {
        super(props);
        this.state = { ...this.props.program, workoutType: this.props.workoutType };

        this._onItemClick = this._onItemClick.bind(this);
        this.webClient = new webClient();
    }

    _onItemClick() {
        this.webClient.post(`Mobile/Workouts/Details/Set/${this.state.trackID}/${this.state.workoutID}/${!this.state.isComplate}`)
            .then(response => {
                this.setState({ isComplate: !this.state.isComplate }, () => {
                    this.props.onItemClick(this.state);
                });
            });
    }

    render() {
        return (
            <View style={{ margin: 5, marginLeft: 0, marginRight: 0, height: 50, justifyContent: "space-between", alignItems: "center", flexDirection: "row" }}>
                <View style={{
                    flex: 1, height: "100%", justifyContent: "space-around",
                    backgroundColor: !this.state.isComplate ? "rgb(230,230,230)" : "rgb(191,212,68)"
                }}>
                    {
                        this.state.workoutType == "weight" ?
                            <View style={{ margin: 5, flexDirection: "row" }}>
                                <Text style={{ fontSize: 12 }}>{this.state.moveName}</Text>
                                <Text style={{ fontSize: 12, fontWeight: "bold" }}>  {this.state.muscleGroups}</Text>
                            </View> :
                            <View style={{ margin: 5, flexDirection: "row" }}>
                                <Text style={{ fontSize: 12 }}>{this.state.exerciseName}</Text>
                            </View>
                    }
                    {
                        this.state.workoutType == "weight" ?
                            <View style={{ margin: 5, flexDirection: "row", justifyContent:"space-between" }}>
                                <Text style={{ fontSize: 12 }}>Ekipman No: {this.state.equipmentNo}</Text>
                                <Text style={{ fontSize: 12 }}>Set: {this.state.numberOfSet}</Text>
                                <Text style={{ fontSize: 12 }}>Tekrar: {this.state.numberOfRepeat}</Text>
                                <Text style={{ fontSize: 12 }}>KG: {this.state.kg}</Text>
                            </View> :
                            <View style={{ margin: 5, flexDirection: "row" }}>
                                <Text style={{ fontSize: 12 }}>Periyod: {this.state.periodMinute} dk</Text>
                                <Text style={{ fontSize: 12 }}>Hız: {this.state.speed}</Text>
                                <Text style={{ fontSize: 12 }}>Rpm: {this.state.rpm}</Text>
                                <Text style={{ fontSize: 12 }}>Incline: {this.state.incline}</Text>
                            </View>
                    }
                </View>
                <TouchableOpacity style={{
                    width: 50, marginLeft: 10, height: "100%", alignItems: "center",
                    backgroundColor: !this.state.isComplete ? "rgb(230,230,230)" : "rgb(191,212,68)"
                }}
                    onPress={this._onItemClick}>
                    {
                        !this.state.isComplate ?
                            <Icon style={{ marginTop: 5, fontSize: 40 }} name="clock" /> :
                            <Icon style={{ marginTop: 5, fontSize: 40 }} name="md-checkmark" />
                    }
                </TouchableOpacity>
            </View>
        );
    }
}
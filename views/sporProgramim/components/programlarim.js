import React, { Component } from 'react';
import { View, TouchableOpacity, ImageBackground } from 'react-native';
import { Text } from 'native-base';


export default class Programlarim extends Component {

    constructor(props) {
        super(props);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            program: nextProps
        });
    }

    render() {
        return (
            <View {...this.props}>
                <Text style={{ margin: 5, fontSize: 16, color: "rgb(127,127,127)" }}>Programlarım</Text>
                <TouchableOpacity style={{
                    margin: 5, height: 60,
                    justifyContent: "space-between", alignItems: "center", flexDirection: "row",
                    backgroundColor: "rgb(242,194,78)",
                }}
                    onPress={() => this.props.navigation.navigate("ProgramlarimCalismaPlanim", { ...this.state.program, workoutType: "cardio" })}>
                    <View>
                        <Text style={{ marginLeft: 5, color: "white", fontSize: 16, fontWeight: "bold" }}>
                            Cardio Programım
                        </Text>
                    </View>
                    <ImageBackground source={require("../../../assets/beyazSagOk.png")}
                        style={{ width: 35, height: 25, marginRight: 25 }} />
                </TouchableOpacity>
                <TouchableOpacity style={{
                    margin: 5, height: 60,
                    justifyContent: "space-between", alignItems: "center", flexDirection: "row",
                    backgroundColor: "rgb(110,180,216)"
                }}
                    onPress={() => this.props.navigation.navigate("ProgramlarimCalismaPlanim", { ...this.state.program, workoutType: "weight" })}>
                    <View>
                        <Text style={{ marginLeft: 5, color: "white", fontSize: 16, fontWeight: "bold" }}>
                            Antrenman Programım
                        </Text>
                    </View>
                    <ImageBackground source={require("../../../assets/beyazSagOk.png")}
                        style={{ width: 35, height: 25, marginRight: 25 }} />
                </TouchableOpacity>
            </View>
        );
    }
}
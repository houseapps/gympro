import React, { Component } from 'react';
import { View, TouchableOpacity, ImageBackground } from 'react-native';
import { Text, Textarea } from 'native-base';


export default class EgitmendenNotlar extends Component {

    constructor() {
        super();
    }

    render() {
        return (
            <View {...this.props}>
                <Text style={{ margin: 5, fontSize: 16, color: "rgb(127,127,127)" }}>Eğitmenden Notlar</Text>
                <Text style={{ margin: 5, fontSize: 14, color: "black" }}>
                    {this.props.trainerNotes}
                </Text>
            </View>
        );
    }
}
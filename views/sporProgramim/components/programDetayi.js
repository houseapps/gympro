import React from "react";
import { View, ImageBackground, TouchableOpacity } from 'react-native';
import { Text } from 'native-base';
import { Divider } from 'react-native-elements';
import { Col, Row, Grid } from 'react-native-easy-grid';

export default class ProgramDetayi extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <View {...this.props}>
                <Grid>
                    <Row style={{ height: 30 }}>
                        <Text style={{ margin: 5, fontSize: 16, color: "rgb(127,127,127)" }}>Program Detayları</Text>
                    </Row>
                    <Row>
                        <Col>
                            <View style={{ flexDirection: "column", alignItems: "center" }}>
                                <Text style={{ margin: 5, fontSize: 14, fontWeight: "bold", color: "rgb(107,179,215)" }}>Program</Text>
                                <Text style={{ margin: 5, fontSize: 14, color: "rgb(127,127,127)" }}>{this.props.programNo}</Text>
                            </View>
                            <Divider style={{ marginTop: 5, marginBottom: 5, backgroundColor: 'lightgray', height: 1, width: "100%" }} />
                            <View style={{ flexDirection: "column", alignItems: "center" }}>
                                <Text style={{ margin: 5, fontSize: 14, fontWeight: "bold", color: "rgb(107,179,215)" }}>Başlangıç Tarihi</Text>
                                <Text style={{ margin: 5, fontSize: 14, color: "rgb(127,127,127)" }}>{this.props.beginDateFormatted}</Text>
                            </View>
                        </Col>
                        <Col>
                            <View style={{ flexDirection: "column", alignItems: "center" }}>
                                <Text style={{ margin: 5, fontSize: 14, fontWeight: "bold", color: "rgb(107,179,215)" }}>Süresi</Text>
                                <Text style={{ margin: 5, fontSize: 14, color: "rgb(127,127,127)" }}>{this.props.numberOfDays} Gün</Text>
                            </View>
                            <Divider style={{ marginTop: 5, marginBottom: 5, backgroundColor: 'lightgray', height: 1, width: "100%" }} />
                            <View style={{ flexDirection: "column", alignItems: "center" }}>
                                <Text style={{ margin: 5, fontSize: 14, fontWeight: "bold", color: "rgb(107,179,215)" }}>Bitiş Tarihi</Text>
                                <Text style={{ margin: 5, fontSize: 14, color: "rgb(127,127,127)" }}>{this.props.endDateFormatted}</Text>
                            </View>
                        </Col>
                        <Col>
                            <View style={{ flexDirection: "column", alignItems: "center" }}>
                                <Text style={{ margin: 5, fontSize: 14, fontWeight: "bold", color: "rgb(107,179,215)" }}>Hedef Nabız</Text>
                                <Text style={{ margin: 5, fontSize: 14, color: "rgb(127,127,127)" }}>112 - 145</Text>
                            </View>
                            <Divider style={{ marginTop: 5, marginBottom: 5, backgroundColor: 'lightgray', height: 1, width: "100%" }} />
                            <View style={{ flexDirection: "column", alignItems: "center" }}>
                                <Text style={{ margin: 5, fontSize: 14, fontWeight: "bold", color: "rgb(107,179,215)" }}>Kalan</Text>
                                <Text style={{ margin: 5, fontSize: 14, color: "rgb(127,127,127)" }}>{this.props.remainingDay} Gün</Text>
                            </View>
                        </Col>
                    </Row>
                </Grid>
            </View>
        );
    }
}
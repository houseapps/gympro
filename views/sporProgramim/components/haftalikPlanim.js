import React, { Component } from 'react';
import { View, TouchableOpacity, ImageBackground } from 'react-native';
import { Text } from 'native-base';


export default class HaftalikPlanim extends Component {

    constructor(props) {
        super(props);
        this._getHaftalikPlanim = this._getHaftalikPlanim.bind(this);

        this.state = {
            haftalikProgram: []
        };
    }

    componentWillReceiveProps(nextProps) {
        let haftalikProgramim = this._getHaftalikPlanim(nextProps);

        this.setState({
            haftalikProgram: haftalikProgramim
        });
    }

    _getHaftalikPlanim(props) {
        var haftalikProgramim = [];

        if (props.monday == 1) {
            haftalikProgramim.push({
                day: 1,
                programID: props.programID,
                programNo: props.programNo,
                header: "Pazartesi",
                weightDetail: props.mondayWorkoutDay > 0 ? `, Gün ${props.mondayWorkoutDay} Ağırlık` : "",
                cardioDetail: props.mondayCardioDay > 0 ? `, Gün ${props.mondayCardioDay} Cardio` : "",
                isWorkoutDay: props.mondayWorkoutDay > 0,
                workoutDay: props.mondayWorkoutDay,
                isCardioDay: props.mondayCardioDay > 0,
                cardioDay: props.mondayCardioDay
            });
        }
        if (props.tuesday == 1) {
            haftalikProgramim.push({
                day: 2,
                programID: props.programID,
                programNo: props.programNo,
                header: "Salı",
                weightDetail: props.tuesdayWorkoutDay > 0 ? `, Gün ${props.tuesdayWorkoutDay} Ağırlık` : "",
                cardioDetail: props.tuesdayCardioDay > 0 ? `, Gün ${props.tuesdayCardioDay} Cardio` : "",
                isWorkoutDay: props.tuesdayWorkoutDay > 0,
                workoutDay: props.tuesdayWorkoutDay,
                isCardioDay: props.tuesdayCardioDay > 0,
                cardioDay: props.tuesdayCardioDay
            });
        }
        if (props.wednesday == 1) {
            haftalikProgramim.push({
                day: 3,
                programID: props.programID,
                programNo: props.programNo,
                header: "Çarşamba",
                weightDetail: props.wednesdayWorkoutDay > 0 ? `, Gün ${props.wednesdayWorkoutDay} Ağırlık` : "",
                cardioDetail: props.wednesdayCardioDay > 0 ? `, Gün ${props.wednesdayCardioDay} Cardio` : "",
                isWorkoutDay: props.wednesdayWorkoutDay > 0,
                workoutDay: props.wednesdayWorkoutDay,
                isCardioDay: props.wednesdayCardioDay > 0,
                cardioDay: props.wednesdayCardioDay
            });
        }
        if (props.thursday == 1) {
            haftalikProgramim.push({
                day: 4,
                programID: props.programID,
                programNo: props.programNo,
                header: "Perşembe",
                weightDetail: props.thursdayWorkoutDay > 0 ? `, Gün ${props.thursdayWorkoutDay} Ağırlık` : "",
                cardioDetail: props.thursdayCardioDay > 0 ? `, Gün ${props.thursdayCardioDay} Cardio` : "",
                isWorkoutDay: props.thursdayWorkoutDay > 0,
                workoutDay: props.thursdayWorkoutDay,
                isCardioDay: props.thursdayCardioDay > 0,
                cardioDay: props.thursdayCardioDay
            });
        }
        if (props.friday == 1) {
            haftalikProgramim.push({
                day: 5,
                programID: props.programID,
                programNo: props.programNo,
                header: "Cuma",
                weightDetail: props.fridayWorkoutDay > 0 ? `, Gün ${props.fridayWorkoutDay} Ağırlık` : "",
                cardioDetail: props.fridayCardioDay > 0 ? `, Gün ${props.fridayCardioDay} Cardio` : "",
                isWorkoutDay: props.fridayWorkoutDay > 0,
                workoutDay: props.fridayWorkoutDay,
                isCardioDay: props.fridayCardioDay > 0,
                cardioDay: props.fridayCardioDay
            });
        }
        if (props.saturday == 1) {
            day: 6,
                haftalikProgramim.push({
                    programID: props.programID,
                    programNo: props.programNo,
                    header: "Cumartesi",
                    weightDetail: props.saturdayWorkoutDay > 0 ? `, Gün ${props.saturdayWorkoutDay} Ağırlık` : "",
                    cardioDetail: props.saturdayCardioDay > 0 ? `, Gün ${props.saturdayCardioDay} Cardio` : "",
                    isWorkoutDay: props.saturdayWorkoutDay > 0,
                    workoutDay: props.saturdayWorkoutDay,
                    isCardioDay: props.saturdayCardioDay > 0,
                    cardioDay: props.saturdayCardioDay
                });
        }
        if (props.sunday == 1) {
            haftalikProgramim.push({
                day: 7,
                programID: props.programID,
                programNo: props.programNo,
                header: "Pazar",
                weightDetail: props.sundayWorkoutDay > 0 ? `, Gün ${props.sundayWorkoutDay} Ağırlık` : "",
                cardioDetail: props.sundayCardioDay > 0 ? `, Gün ${props.sundayCardioDay} Cardio` : "",
                isWorkoutDay: props.sundayWorkoutDay > 0,
                workoutDay: props.sundayWorkoutDay,
                isCardioDay: props.sundayCardioDay > 0,
                cardioDay: props.sundayCardioDay
            });
        }

        return haftalikProgramim;
    }

    render() {
        return (
            <View {...this.props}>
                <Text style={{ margin: 5, fontSize: 16, color: "rgb(127,127,127)" }}>Haftalık Planım</Text>
                {this.state.haftalikProgram.map((program, index) => {
                    return (
                        <TouchableOpacity key={index} style={{
                            margin: 5, height: 60,
                            justifyContent: "space-between", alignItems: "center", flexDirection: "row",
                            backgroundColor: "rgb(238,143,77)"
                        }}
                            onPress={() => this.props.navigation.navigate("HaftalikPlanimDetay", program)}>
                            <View>
                                <Text style={{ marginLeft: 5, color: "white", fontSize: 16, fontWeight: "bold" }}>
                                    {program.header}
                                </Text>
                                <Text style={{ marginLeft: 5, marginTop: 5, color: "white", fontSize: 12, fontWeight: "bold" }}>
                                    Antrenman{program.weightDetail + program.cardioDetail}
                                </Text>
                            </View>
                            <ImageBackground source={require("../../../assets/beyazSagOk.png")}
                                style={{ width: 35, height: 25, marginRight: 25 }} />
                        </TouchableOpacity>
                    )
                })}
            </View>
        );
    }
}
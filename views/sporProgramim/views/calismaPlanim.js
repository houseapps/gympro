import React, { Component } from 'react';
import { View, TouchableOpacity, ImageBackground, ScrollView } from 'react-native';
import { Text, Textarea, Icon } from 'native-base';

import CalismaPlanimItem from '../components/calismaPlanimItem';
import HeaderBar from '../../HeaderBar';
import webClient from '../../../helper/webclient';
import index from 'axios';

const title = "Spor Programım";

export default class CalismaPlanim extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor(props) {
        super(props);
        this.state = {
            isAnladimClick: false,
            programData: []
        }

        this._onItemClick = this._onItemClick.bind(this);
        this.webClient = new webClient();

        let type = this.props.navigation.state.params.workoutType;
        let day = -1;
        if (type == "weight") {
            day = this.props.navigation.state.params.workoutDay;
        }
        else {
            day = this.props.navigation.state.params.cardioDay;
        }
        this.webClient.get(`Mobile/Workouts/Details/${this.props.navigation.state.params.programID}/${day}/${type}`)
            .then(response => {
                this.setState({
                    programData: response.data.data.workout,
                    workoutType: this.props.navigation.state.params.workoutType
                })
            });
    }

    _onItemClick(item) {
        var newProgramData = this.state.programData.map(x => x.workoutID != item.workoutID ? x : item);
        this.setState({ programData: newProgramData });
    }

    render() {
        let totalElementCount = this.state.programData.length;
        let completedElementCount = this.state.programData.filter(x => x.isComplate).length;
        let percentage = Number.parseInt(completedElementCount * 100 / totalElementCount);
        return (
            <View style={{ width: "100%", height: "100%" }}>
                {
                    !this.state.isAnladimClick ?
                        <View style={{ margin: 10, height: 120, backgroundColor: "rgb(230,230,230)" }}>
                            <View style={{ margin: 5, marginTop: 20, flexDirection: "row", alignItems: "center" }}>
                                <Icon style={{ fontSize: 48 }} name="clock" />
                                <Text style={{ margin: 5, marginLeft: 10, marginRight: 15, fontSize: 14, fontWeight: "500", color: "rgb(127,127,127)" }}>
                                    İkonuna tıklayarak yaptığınız antrenmanları işaretleyin
                    </Text>
                            </View>
                            <TouchableOpacity style={{ position: "absolute", bottom: 5, right: 5 }}
                                onPress={() => this.setState({ isAnladimClick: true })}>
                                <Text style={{ margin: 5, fontSize: 14, fontWeight: "500", color: "rgb(127,127,127)" }}>
                                    Anladım
                        </Text>
                            </TouchableOpacity>
                        </View> :
                        null
                }

                <View style={{ margin: 10, marginTop: 5 }}>
                    <Text>
                        {this.props.navigation.state.params.header}{
                            this.props.navigation.state.params.workoutType == "weight" ?
                                this.props.navigation.state.params.weightDetail :
                                this.props.navigation.state.params.cardioDetail}
                    </Text>
                </View>
                <View>
                    <View style={{ marginRight: 10, flexDirection: "row", justifyContent: "flex-end" }}>
                        <Text style={{ fontSize: 18, color: "rgb(65,159,221)" }}>%{percentage} Tamamlandı</Text>
                    </View>
                    <View style={{ height: 30, marginLeft: 10, marginRight: 10, marginTop: 5, flexDirection: "row" }}>
                        <View style={{ width: `${percentage}%`, backgroundColor: "rgb(78,180,246)" }} />
                        <View style={{ width: `${100 - percentage}%`, backgroundColor: "rgb(230,230,230)" }} />
                    </View>
                </View>
                <ScrollView style={{ margin: 10, marginTop: 5 }} showsVerticalScrollIndicator={false}>
                    {this.state.programData.map((program, index) => {
                        return (
                            <CalismaPlanimItem key={program.workoutID} index={index + 1} program={program} workoutType={this.state.workoutType} onItemClick={this._onItemClick} />
                        )
                    })}
                </ScrollView>
            </View>
        );
    }
}
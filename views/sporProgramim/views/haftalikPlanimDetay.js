import React, { Component } from 'react';
import { View, TouchableOpacity, ImageBackground } from 'react-native';
import { Text, Textarea } from 'native-base';

import HeaderBar from '../../HeaderBar';

const title = "Spor Programım";

export default class HaftalikPlanimDetay extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor(props) {
        super(props);
        
        this.state = {
            program: this.props.navigation.state.params
        }
    }

    render() {
        return (
            <View {...this.props}>
                <Text style={{ margin: 5, fontSize: 16, color: "rgb(127,127,127)" }}>{this.state.program.header}</Text>
                {
                    this.state.program.isWorkoutDay ?
                        <TouchableOpacity style={{
                            margin: 5, height: 60,
                            justifyContent: "space-between", alignItems: "center", flexDirection: "row",
                            backgroundColor: "rgb(110,180,216)"
                        }}
                            onPress={() => this.props.navigation.navigate("CalismaPlanim", { ...this.state.program, workoutType: "weight" })}>
                            <View>
                                <Text style={{ marginLeft: 5, color: "white", fontSize: 16, fontWeight: "bold" }}>
                                    Ağırlık
                            </Text>
                            </View>
                            <ImageBackground source={require("../../../assets/beyazSagOk.png")}
                                style={{ width: 35, height: 25, marginRight: 25 }} />
                        </TouchableOpacity>
                        : null
                }
                {
                    this.state.program.isCardioDay ?
                        <TouchableOpacity style={{
                            margin: 5, height: 60,
                            justifyContent: "space-between", alignItems: "center", flexDirection: "row",
                            backgroundColor: "rgb(110,180,216)"
                        }}
                            onPress={() => this.props.navigation.navigate("CalismaPlanim", { ...this.state.program, workoutType: "cardio" })}>
                            <View>
                                <Text style={{ marginLeft: 5, color: "white", fontSize: 16, fontWeight: "bold" }}>
                                    Cardio
                            </Text>
                            </View>
                            <ImageBackground source={require("../../../assets/beyazSagOk.png")}
                                style={{ width: 35, height: 25, marginRight: 25 }} />
                        </TouchableOpacity>
                        : null
                }
            </View>
        );
    }
}
import React, { Component } from 'react';
import { View, TouchableOpacity, ImageBackground, ScrollView } from 'react-native';
import { Text, Textarea, Icon, List, ListItem, Left, Body, Right } from 'native-base';

import CalismaPlanimItem from '../components/calismaPlanimItem';
import HeaderBar from '../../HeaderBar';
import webClient from '../../../helper/webclient';

const title = "Spor Programım";

export default class ProgramlarimCalismaPlanim extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor(props) {
        super(props);
        this.state = {
            isAnladimClick: false,
            workoutType: "",
            programData: []
        }

        this.webClient = new webClient();
        let subSystem = this.props.navigation.state.params.workoutType == "weight" ? "WeightProgram" : "CardioProgram";
        this.webClient.get(`Mobile/Workouts/${subSystem}/${this.props.navigation.state.params.programID}`)
            .then(response => {
                this.setState({
                    programData: response.data.data,
                    workoutType: this.props.navigation.state.params.workoutType
                })
            });
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%" }}>
                <Text style={{ marginLeft: 10, fontSize: 18, fontWeight: "400", color: "rgb(65,159,221)" }}>
                    {this.state.workoutType == "weight" ? "Tüm Ağırlık Programım" : "Tüm Cardio Programım"}
                </Text>
                <ScrollView style={{ margin: 10, marginTop: 5 }} showsVerticalScrollIndicator={false}>
                    <List style={{ marginLeft: -10 }}>
                        {this.state.programData.map((program, index) => {
                            return (
                                this.props.navigation.state.params.workoutType == "weight" ?
                                    <ListItem key={index}>
                                        <Body>
                                            <View style={{ margin: 2, flexDirection: "column" }}>
                                                <Text note>{program.muscleGroups}</Text>
                                                <Text note style={{ fontWeight: "bold" }}>{program.moveName}</Text>
                                            </View>
                                            <View style={{ margin: 2, flexDirection: "row" }}>
                                                <Text style={{ margin: 2 }} note>Ekipman No: {program.equipmentNo}</Text>
                                                <Text style={{ margin: 2 }} note>Set: {program.numberOfSet}</Text>
                                                <Text style={{ margin: 2 }} note>Tekrar : {program.numberOfRepeat}</Text>
                                            </View>
                                        </Body>
                                        <Right>
                                            <Text style={{ marginTop: -30 }} note>Gün: {program.workoutDay}</Text>
                                        </Right>
                                    </ListItem> :
                                    <ListItem key={index}>
                                        <Body>
                                            <Text style={{ margin: 2, fontWeight: "bold" }}>{program.exerciseName}</Text>
                                            <View style={{ margin: 2, flexDirection: "row" }}>
                                                <Text style={{ margin: 2 }} note>Hız: {program.speed}</Text>
                                                <Text style={{ margin: 2 }} note>RPM: {program.speed}</Text>
                                                <Text style={{ margin: 2 }} note>Eğitim: {program.speed}</Text>
                                                <Text style={{ margin: 2 }} note>Süre: {program.speed}</Text>
                                            </View>
                                        </Body>
                                        <Right>
                                            <Text style={{ marginTop: -30 }} note>Gün: {program.numberOfDay}</Text>
                                        </Right>
                                    </ListItem>
                            )
                        })}
                    </List>
                </ScrollView>
            </View>
        );
    }
}
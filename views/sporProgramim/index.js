import React, { Component } from 'react';
import { View, ScrollView, Platform } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Button, Text, Thumbnail, Picker, Item, Icon } from 'native-base';
import { Divider } from 'react-native-elements';
import { Grid, Row, Col } from 'react-native-easy-grid';

import ProgramDetayi from './components/programDetayi';
import HaftalikPlanim from './components/haftalikPlanim';
import Programlarim from './components/programlarim';
import EgitmendenNotlar from './components/egitmendenNotlar';
import HeaderBar from '../HeaderBar'
import webClient from '../../helper/webclient';

const title = "Spor Programım";

export default class Index extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            selectedData: {},
            selectedDateIndex: 0,
            dates: []
        }
        this.webClient = new webClient();
    }

    componentWillMount() {
        this.webClient.get(`Mobile/Workouts`)
            .then(response => {
                var dates = response.data.data.map(x => x.beginDateFormatted);
                this.setState({
                    data: response.data.data,
                    selectedData: response.data.data[0],
                    selectedDateIndex: 0,
                    dates: dates
                })
            }).catch(error => this.props.navigation.goBack());
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%" }}>
                <View style={{ height: 50, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                    <Text style={{ marginLeft: 10, marginRight: 10, fontSize: 18, fontWeight: "400", color: "rgb(60, 148,214)" }}>Spor Programı Seç</Text>
                    <View
                        style={{ backgroundColor: "rgb(65,159,221)", borderRadius: 5, flexDirection: "row", alignItems: "center", marginRight: 5, height: 40 }}>
                        <Picker info
                            style={{ margin: 7, marginRight: 10, width: (Platform.OS === 'ios') ? undefined : 200 }}
                            textStyle={{ color: 'white' }}
                            iosHeader="Seçim yapınız"
                            headerBackButtonText="Geri"
                            mode="dropdown"
                            selectedValue={this.state.selectedDateIndex}
                            onValueChange={(index) => { this.setState({ selectedDateIndex: index, selectedData: this.state.data[index] }) }}>
                            {
                                this.state.dates.map((date, index) => {
                                    return <Item key={index} label={date} value={index} />
                                })
                            }
                        </Picker>
                        {
                            Platform.OS == "ios" ?
                                <Icon style={{ marginLeft: -10, marginRight: 10, fontSize: 14, color: "white" }} name="ios-arrow-down" />
                                : null
                        }
                    </View>
                </View>
                <ScrollView>
                    <LinearGradient colors={["rgb(107,179,215)", "rgb(66,132,181)"]}>
                        <View style={{
                            height: 180, flexDirection: "column",
                            alignItems: "center", justifyContent: "space-between"
                        }}>
                            <Thumbnail large style={{ marginTop: 10, backgroundColor: "transparent" }}
                                source={{ uri: "data:image/png;base64," + this.state.selectedData.fitnessConsultantPicture }} />
                            <Text style={{ color: "white", backgroundColor: "transparent" }}>
                                EĞİTMEN : {this.state.selectedData.fitnessConsultantName + " " + this.state.selectedData.fitnessConsultantSurName}
                            </Text>
                            <Button full style={{ margin: 10, borderRadius: 5, backgroundColor: "rgb(191,212,68)" }}
                                onPress={() => this.props.navigation.navigate("MesajGonder", { type: "SporProgramim", ...this.state.selectedData })}>
                                <Text style={{ color: "white" }}>Mesaj Gönder</Text>
                            </Button>
                        </View>
                    </LinearGradient>
                    <ProgramDetayi {...this.state.selectedData} />
                    <Divider style={{ marginTop: 5, marginBottom: 5, backgroundColor: 'lightgray', height: 1, width: "100%" }} />
                    <HaftalikPlanim {...this.state.selectedData} navigation={this.props.navigation} />
                    <Programlarim {...this.state.selectedData} navigation={this.props.navigation} />
                    <EgitmendenNotlar {...this.state.selectedData} />
                </ScrollView>
            </View>
        );
    }
}
import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import { Tab, Tabs, TabHeading, Text, Button } from 'native-base';
import { Divider } from 'react-native-elements';

import HeaderBar from '../HeaderBar';
import WebClient from '../../helper/webclient';
import UyelikOzetItem from './components/uyelikOzetItem';

const title = "Üyeliklerim";

export default class Index extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderBar navigation={navigation} title={title} />
    });

    constructor() {
        super();
        this.webClient = new WebClient();

        this.state = {
            pasifUyelikler: [],
            aktifUyelikler: []
        }
    }

    componentWillMount() {
        setTimeout(() => {
            this.webClient.get(`Mobile/Memberships/0`)
                .then(response => {
                    this.setState({
                        pasifUyelikler: response.data.data
                    });
                    this.webClient.get(`Mobile/Memberships/1`)
                        .then(response2 => {
                            this.setState({
                                aktifUyelikler: response2.data.data
                            });
                        });
                 });
        }, 500);
    }

    render() {
        return (
            <View style={{ width: "100%", height: "100%" }}>
                <Tabs locked={true} initialPage={0} tabBarUnderlineStyle={{ backgroundColor: "rgb(100,100,100)" }}>
                    <Tab heading={<TabHeading style={{ backgroundColor: "rbg(248,248,248)" }}><Text style={{ color: "rgb(111,111,111)", fontSize: 14 }}>AKTİF ÜYELİKLERİM</Text></TabHeading>}>
                        <ScrollView style={{ width: "100%", height: "100%" }} showsVerticalScrollIndicator={false}>
                            {
                                this.state.aktifUyelikler.map((item, index) =>
                                    <View key={index}>
                                        <UyelikOzetItem {...item} />
                                        <Divider style={{ backgroundColor: 'rgb(158,158,158)', height: 1, width: "100%" }} />
                                    </View>
                                )
                            }
                        </ScrollView>
                    </Tab>
                    <Tab heading={<TabHeading style={{ backgroundColor: "rbg(248,248,248)" }}><Text style={{ color: "rgb(111,111,111)", fontSize: 14 }}>PASİF ÜYELİKLERİM</Text></TabHeading>}>
                        <ScrollView style={{ width: "100%", height: "100%" }} showsVerticalScrollIndicator={false}>
                            {
                                this.state.pasifUyelikler.map((item, index) =>
                                    <View key={index}>
                                        <UyelikOzetItem {...item} />
                                        <Divider style={{ backgroundColor: 'rgb(158,158,158)', height: 1, width: "100%" }} />
                                    </View>
                                )
                            }
                        </ScrollView>
                    </Tab>
                </Tabs>
            </View>
        );
    }
}
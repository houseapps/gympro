import React, { Component } from 'react';
import { View } from 'react-native';

import { Text } from 'native-base'
import { Col, Row, Grid } from 'react-native-easy-grid';
import ProgressCircle from 'react-native-progress-circle';

export default class UyelikOzetItem extends Component {
    constructor() {
        super();
    }

    render() {
        var kalanPercent = this.props.numberOfDays * 100 / this.props.numberOfTotalDays;
        return (
            <View style={{ flexDirection: "column", alignItems: "center", height: 175 }}>
                <View style={{ margin: 10 }}>
                    <Text style={{ fontSize: 14, fontWeight: "bold" }}>{this.props.membershipType}</Text>
                </View>
                <View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ margin: 5 }}>
                            <ProgressCircle
                                borderWidth={15}
                                radius={60}
                                percent={100 - kalanPercent}
                                shadowColor="rgb(110,180,216)"
                                color="rgb(246,204,70)"
                                innerColor="white"
                                bgColor="white">
                                <View style={{ flex: 1, justifyContent: "center" }}>
                                    <Text style={{ fontWeight: "normal", textAlign: "center", fontSize: 14 }}>{this.props.remaingInfo}</Text>
                                </View>
                            </ProgressCircle>
                        </View>
                        <View style={{ margin: 5 }}>
                            <View style={{ flex: 1, flexDirection: "column" }}>
                                <View style={{ margin: 5 }}>
                                    <Text style={{ fontWeight: "bold", fontSize: 12 }}>Başlangıç: {this.props.beginningDateFormatted}</Text>
                                    <Text style={{ fontWeight: "bold", fontSize: 12 }}>Bitiş: {this.props.endDateFormatted}</Text>
                                    <Text style={{ fontWeight: "bold", fontSize: 12 }}>Fiyat: {this.props.membershipAmountFormatted}</Text>
                                </View>
                                <View style={{ margin: 5 }}>
                                    <Text style={{ fontWeight: "normal", fontSize: 12 }}>Ödenen: {this.props.paidAmountFormatted}</Text>
                                    <Text style={{ fontWeight: "normal", fontSize: 12 }}>Kalan: {this.props.remainingAmountFormatted}</Text>
                                    <Text style={{ fontWeight: "normal", fontSize: 12 }}>Ödeme Vadesi: {this.props.paymentTerm}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}
import Axios from 'axios';

import * as AppActions from '../redux/actions/app';

let accessToken = undefined;
let branchId = undefined;

export default class WebClient {

    constructor(defaultConfig) {
        if (accessToken) {
            defaultConfig = {
                baseURL: "https://api.fitnessonline.net/v1",
                headers: { 'Authorization': "Bearer " + accessToken }
            };
        }

        this.axiosInstance = Axios.create(defaultConfig);
    }

    _getPathWithBranchId(path) {
        if (branchId) {
            var paths = path.split('/');
            path = paths[0] + "/" + branchId + "/" + paths[1];
            for (let index = 2; index < paths.length; index++) {
                path += "/" + paths[index];
            }
        }

        return path;
    }

    get(path, handleRequest = true, handleBranchId = true) {
        path = handleBranchId ? this._getPathWithBranchId(path) : path;
        var realPromise = this.axiosInstance.get(path);
        return this.createProxyPromise(realPromise, handleRequest);
    }

    delete(path, handleRequest = true, handleBranchId = true) {
        path = handleBranchId ? this._getPathWithBranchId(path) : path;
        var realPromise = this.axiosInstance.delete(path);
        return this.createProxyPromise(realPromise, handleRequest);
    }

    post(path, data, handleRequest = true, handleBranchId = true) {
        path = handleBranchId ? this._getPathWithBranchId(path) : path;
        var realPromise = this.axiosInstance.post(path, data);
        return this.createProxyPromise(realPromise, handleRequest);
    }

    put(path, data, handleRequest = true, handleBranchId = true) {
        path = handleBranchId ? this._getPathWithBranchId(path) : path;
        var realPromise = this.axiosInstance.put(path, data);
        return this.createProxyPromise(realPromise, handleRequest);
    }

    patch(path, data, handleRequest = true, handleBranchId = true) {
        path = handleBranchId ? this._getPathWithBranchId(path) : path;
        var realPromise = this.axiosInstance.patch(path, data);
        return this.createProxyPromise(realPromise, handleRequest);
    }

    createProxyPromise(realPromise, handleRequest) {
        if (handleRequest) {
            AppActions.isLoading();
        }
        var _self = this;
        var proxyPromise = new Promise(function (resolve, reject) {
            realPromise.then(function (data) {
                if (handleRequest) {
                    _self.responseHandler(data, resolve, reject);
                }
                else {
                    resolve(data);
                }
            })
                .catch(function (data) {
                    if (handleRequest) {
                        AppActions.isLoaded();
                        _self.errorHandler(data, reject);
                    }
                    else {
                        reject(data);
                    }
                });
        });
        return proxyPromise;
    }

    responseHandler(response, callback, reject) {
        AppActions.isLoaded();
        if (response.data && response.data.isSuccessful != undefined && !response.data.isSuccessful) {
            if (response.data.errorCode == 300 || response.data.errorCode == 100) {
                callback(response);
            }
            else {
                AppActions.alert("Uyarı", response.data.errorMessage);
                reject(response);
            }
        }
        else {
            callback(response);
        }
    }

    errorHandler(response, callback) {
        AppActions.isLoaded();
        if (response.response === undefined) {
            // AppActions.alert(response.message, response.stack);
            AppActions.alert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.");
        }
        else if (response.response.data && response.response.data.isSuccessful != undefined && !response.response.data.isSuccessful) {
            AppActions.alert("Uyarı", response.response.data.errorMessage);
        }
        else {
            AppActions.alert(response.response.data.error, response.message);
        }
        callback(response);
    }
}

export function setAccessToken(token) {
    accessToken = token;
}

export function setBranchId(id) {
    branchId = id;
}